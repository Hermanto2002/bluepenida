<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/bookingpage', function () {
//     return view('user.checkbooking');
// });

//user
Route::get('/', 'user\UserController@index')->name('index');

//user-blog
Route::get('/blog', 'user\BlogController@index')->name('blog-index');
Route::get('/blog/{slug}', 'user\BlogController@show')->name('blog-show');
Route::get('/blog/category/{id}', 'user\BlogController@category')->name('blog-category');
Route::get('/blog/search', 'user\BlogController@search')->name('blog-search');
//cart
Route::get('/cart', 'user\CartController@index')->name('cart-index');
Route::post('/cart/boat/{id}', 'user\CartController@addToCartBoat')->name('cart-boat');
Route::post('/cart/car/{id}', 'user\CartController@addToCartCar')->name('cart-car');
Route::get('/cart/boat/delete/{id}', 'user\CartController@deleteCartBoat')->name('cart-boat-delete');
Route::get('/cart/car/delete/{id}', 'user\CartController@deleteCartCar')->name('cart-car-delete');
Route::post('/booking', 'user\CartController@saveCart')->name('cart-save');
Route::get('/checkbooking', 'user\CartController@checkbooking')->name('checkbooking');
Route::post('/checkbookingresult', 'user\CartController@checkbookingresult')->name('checkbookingresult');

//search
Route::get('/search/boat', 'user\SearchController@searchboat')->name('searchboat');
Route::get('/search/car', 'user\SearchController@searchcar')->name('searchcar');
Route::get('/car', 'user\SearchController@car')->name('car');
Route::get('/boat', 'user\SearchController@boat')->name('boat');

Route::middleware('auth')->group(function(){
	Route::get('admin/', 'admin\HomeController@index')->name('admin.home');
	//admin-data
	Route::get('admin/data/resetpass', 'admin\DataController@edit')->name('admin.data.edit');
	Route::post('admin/data/update', 'admin\DataController@update')->name('admin.data.update');
	// admin-blog
	Route::get('admin/blog', 'admin\BlogController@index')->name('admin.blog');
	Route::get('admin/blog/create', 'admin\BlogController@create')->name('admin.blog.create');
	Route::post('admin/blog/store', 'admin\BlogController@store')->name('admin.blog.store');
	Route::get('admin/blog/edit/{id}', 'admin\BlogController@edit')->name('admin.blog.edit');
	Route::post('admin/blog/update', 'admin\BlogController@update')->name('admin.blog.update');
	Route::get('admin/blog/delete/{id}', 'admin\BlogController@destroy')->name('admin.blog.destroy');
	// admin-blog-category
	Route::get('admin/blog/category', 'admin\CategoryController@index')->name('admin.blog.category');
	Route::get('admin/blog/category/create', 'admin\CategoryController@create')->name('admin.blog.category.create');
	Route::post('admin/blog/category/store', 'admin\CategoryController@store')->name('admin.blog.category.store');
	Route::get('admin/blog/category/edit/{id}', 'admin\CategoryController@edit')->name('admin.blog.category.edit');
	Route::post('admin/blog/category/update', 'admin\CategoryController@update')->name('admin.blog.category.update');
	Route::get('admin/blog/category/delete/{id}', 'admin\CategoryController@destroy')->name('admin.blog.category.destroy');
	// admin-post-boat
	Route::get('admin/boat', 'admin\PostboatController@index')->name('admin.boat');
	Route::get('admin/boat/create', 'admin\PostboatController@create')->name('admin.boat.create');
	Route::post('admin/boat/store', 'admin\PostboatController@store')->name('admin.boat.store');
	Route::get('admin/boat/edit/{id}', 'admin\PostboatController@edit')->name('admin.boat.edit');
	Route::post('admin/boat/update', 'admin\PostboatController@update')->name('admin.boat.update');
	Route::get('admin/boat/delete/{id}', 'admin\PostboatController@destroy')->name('admin.boat.destroy');
	// admin-post-boat-price
	Route::get('admin/boat-price', 'admin\BoatpriceController@index')->name('admin.boatprice');
	Route::get('admin/boat-price/create', 'admin\BoatpriceController@create')->name('admin.boatprice.create');
	Route::post('admin/boat-price/store', 'admin\BoatpriceController@store')->name('admin.boatprice.store');
	Route::get('admin/boat-price/edit/{id}', 'admin\BoatpriceController@edit')->name('admin.boatprice.edit');
	Route::post('admin/boat-price/update', 'admin\BoatpriceController@update')->name('admin.boatprice.update');
	Route::get('admin/boat-price/delete/{id}', 'admin\BoatpriceController@destroy')->name('admin.boatprice.destroy');
	// admin-post-car
	Route::get('admin/car', 'admin\PostcarController@index')->name('admin.car');
	Route::get('admin/car/create', 'admin\PostcarController@create')->name('admin.car.create');
	Route::post('admin/car/store', 'admin\PostcarController@store')->name('admin.car.store');
	Route::get('admin/car/edit/{id}', 'admin\PostcarController@edit')->name('admin.car.edit');
	Route::post('admin/car/update', 'admin\PostcarController@update')->name('admin.car.update');
	Route::get('admin/car/delete/{id}', 'admin\PostcarController@destroy')->name('admin.car.destroy');
	// admin-post-boat-price
	Route::get('admin/car-price', 'admin\CarpriceController@index')->name('admin.carprice');
	Route::get('admin/car-price/create', 'admin\CarpriceController@create')->name('admin.carprice.create');
	Route::post('admin/car-price/store', 'admin\CarpriceController@store')->name('admin.carprice.store');
	Route::get('admin/car-price/edit/{id}', 'admin\CarpriceController@edit')->name('admin.carprice.edit');
	Route::post('admin/car-price/update', 'admin\CarpriceController@update')->name('admin.carprice.update');
	Route::get('admin/car-price/delete/{id}', 'admin\CarpriceController@destroy')->name('admin.carprice.destroy');
	// admin-post-trip
	Route::get('admin/trip', 'admin\TripController@index')->name('admin.trip');
	Route::get('admin/trip/create', 'admin\TripController@create')->name('admin.trip.create');
	Route::post('admin/trip/store', 'admin\TripController@store')->name('admin.trip.store');
	Route::get('admin/trip/edit/{id}', 'admin\TripController@edit')->name('admin.trip.edit');
	Route::post('admin/trip/update', 'admin\TripController@update')->name('admin.trip.update');
	Route::get('admin/trip/delete/{id}', 'admin\TripController@destroy')->name('admin.trip.destroy');
	// admin-post-localtions
	Route::get('admin/localtions', 'admin\LocaltionsController@index')->name('admin.localtions');
	Route::get('admin/localtions/create', 'admin\LocaltionsController@create')->name('admin.localtions.create');
	Route::post('admin/localtions/store', 'admin\LocaltionsController@store')->name('admin.localtions.store');
	Route::get('admin/localtions/edit/{id}', 'admin\LocaltionsController@edit')->name('admin.localtions.edit');
	Route::post('admin/localtions/update', 'admin\LocaltionsController@update')->name('admin.localtions.update');
	Route::get('admin/localtions/delete/{id}', 'admin\LocaltionsController@destroy')->name('admin.localtions.destroy');
	// admin-booking
	Route::get('admin/booking', 'admin\BookingController@index')->name('admin.booking');
	Route::get('admin/booking/create', 'admin\BookingController@create')->name('admin.booking.create');
	Route::post('admin/booking/store', 'admin\BookingController@store')->name('admin.booking.store');
	Route::get('admin/booking/edit', 'admin\BookingController@edit')->name('admin.booking.edit');
	Route::post('admin/booking/update', 'admin\BookingController@update')->name('admin.booking.update');
	Route::get('admin/booking/delete/{id}', 'admin\BookingController@destroy')->name('admin.booking.destroy');
	//admin-page-menu
	Route::get('admin/menu', 'admin\MenuController@index')->name('admin.menu');
	Route::get('admin/menu/create', 'admin\MenuController@create')->name('admin.menu.create');
	Route::post('admin/menu/store', 'admin\MenuController@store')->name('admin.menu.store');
	Route::get('admin/menu/edit/{id}', 'admin\MenuController@edit')->name('admin.menu.edit');
	Route::post('admin/menu/update', 'admin\MenuController@update')->name('admin.menu.update');
	Route::get('admin/menu/delete/{id}', 'admin\MenuController@destroy')->name('admin.menu.destroy');
	//admin-page-general
	Route::get('admin/general', 'admin\PagegeneralController@index')->name('admin.general');
	Route::get('admin/general/create', 'admin\PagegeneralController@create')->name('admin.general.create');
	Route::post('admin/general/store', 'admin\PagegeneralController@store')->name('admin.general.store');
	Route::get('admin/general/edit', 'admin\PagegeneralController@edit')->name('admin.general.edit');
	Route::post('admin/general/update', 'admin\PagegeneralController@update')->name('admin.general.update');
	Route::get('admin/general/delete/{id}', 'admin\PagegeneralController@destroy')->name('admin.general.destroy');
	//admin-page-contact
	Route::get('admin/contact', 'admin\PagecontactController@index')->name('admin.contact');
	Route::get('admin/contact/create', 'admin\PagecontactController@create')->name('admin.contact.create');
	Route::post('admin/contact/store', 'admin\PagecontactController@store')->name('admin.contact.store');
	Route::get('admin/contact/edit', 'admin\PagecontactController@edit')->name('admin.contact.edit');
	Route::post('admin/contact/update', 'admin\PagecontactController@update')->name('admin.contact.update');
	Route::get('admin/contact/delete/{id}', 'admin\PagecontactController@destroy')->name('admin.contact.destroy');
	//admin-page-slider
	Route::get('admin/slider', 'admin\PagesliderController@index')->name('admin.slider');
	Route::get('admin/slider/create', 'admin\PagesliderController@create')->name('admin.slider.create');
	Route::post('admin/slider/store', 'admin\PagesliderController@store')->name('admin.slider.store');
	Route::get('admin/slider/edit/{id}', 'admin\PagesliderController@edit')->name('admin.slider.edit');
	Route::post('admin/slider/update', 'admin\PagesliderController@update')->name('admin.slider.update');
	Route::get('admin/slider/delete/{id}', 'admin\PagesliderController@destroy')->name('admin.slider.destroy');

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
