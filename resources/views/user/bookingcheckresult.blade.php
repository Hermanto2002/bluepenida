@include('includeuser.head')
@include('includeuser.header')

<section id="home" class="about-us-search">
	<div class="container">
		<div class="about-us-content">
			<div class="row">

			</div>
			<!--/.row-->
		</div>
		<!--/.about-us-content-->
	</div>
	<!--/.container-->

</section>

<div class="container">
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">Booking Code</th>
				<th scope="col">Price</th>
				<th scope="col">Email</th>
				<th scope="col">Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach($result as $item)
			<tr>
				<td>{{$item->nama}}</td>
				<td>{{$item->booking_code}}</td>
				<td>{{$item->price}}</td>
				<td>{{$item->email}}</td>
				<td>{{$item->Status}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@include('includeuser.footer')
@include('includeuser.foot')