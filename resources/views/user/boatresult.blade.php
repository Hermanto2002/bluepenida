@include('includeuser.head')
@include('includeuser.header')


<section id="home" class="about-us-search">
    <div class="container">
        <div class="about-us-content">
            <div class="row">

            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->
<!-- lorem -->
<!--travel-box start-->
@include('includeuser.search')

<!--/.travel-box-->
<section>
    <div class="container">
        <div class="mx-3">
            @if(count($result))
            <div class="alert alert-info" role="alert">
                <h4 class="mt-4">
                    Search Result
                </h4>
            </div>
            @else
            <div class="alert alert-info" role="alert">
                <h4 class="mt-4">
                    No Result
                </h4>
            </div>
            @endif
            <div class="row">
                @foreach($result as $item)
                <div class="col-sm-12 mt-2">
                    <div class="single-package-item sticky-top pt-3">
                        <div class="single-package-item-txt">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img width="50%" src="{{ asset('images/boat/'.$item->getBoat->image)}}" alt="">
                                </div>
                                <div class="col-sm-6">
                                    <h3>{{ $item->getBoat->title }}</h3>
                                    <h4 class="mt-3">{{$item->time_departing}} -> {{$item->time_arrived}}</h4>
                                    <h4 class="mt-1">{{$item->getFrom->name}} -> {{$item->getTo->name}}</h4>
                                    <br>
                                    @if($item->getBoat->life_jacket=='on')
                                    <i data-toggle="tooltip" data-placement="top" title="life jacket"
                                        class="material-icons">filter_tilt_shift
                                    </i>
                                    @endif
                                    @if($item->getBoat->baggage=='on')
                                    <i data-toggle="tooltip" data-placement="top" title="baggage"
                                        class="material-icons">
                                        business_center
                                    </i>
                                    @endif
                                    @if($item->getBoat->insurance=='on')
                                    <i data-toggle="tooltip" data-placement="top" title="insurance"
                                        class="material-icons">favorite
                                    </i>
                                    @endif
                                    @if($item->getBoat->tax=='on')
                                    <i data-toggle="tooltip" data-placement="top" title="tax"
                                        class="material-icons">local_atm
                                    </i>
                                    @endif
                                    <br>
                                    <a type="button" class="mt-1 btn-link" data-toggle="modal"
                                        data-target="#detail{{$item->id}}">
                                        Detail & policy</a>

                                    <!-- Modal -->
                                    <div class="modal fade" id="detail{{$item->id}}" tabindex="-1" role="dialog"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <img width="50%"
                                                                src="{{ asset('images/boat/'.$item->getBoat->image)}}"
                                                                alt="">
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <h3>{{ $item->getBoat->title }}</h3>
                                                            <h4 class="mt-3">{{$item->time_departing}} ->
                                                                {{$item->time_arrived}}</h4>
                                                            <h4 class="mt-1">{{$item->getFrom->name}} ->
                                                                {{$item->getTo->name}}</h4>
                                                            <br>
                                                            @if($item->getBoat->life_jacket=='on')
                                                            <i data-toggle="tooltip" data-placement="top"
                                                                title="life jacket"
                                                                class="material-icons">filter_tilt_shift
                                                            </i><span>life jacket</span>
                                                            @endif
                                                            <br>
                                                            @if($item->getBoat->baggage=='on')
                                                            <i data-toggle="tooltip" data-placement="top"
                                                                title="baggage" class="material-icons">
                                                                business_center
                                                            </i><span>baggage</span>
                                                            @endif
                                                            <br>
                                                            @if($item->getBoat->insurance=='on')
                                                            <i data-toggle="tooltip" data-placement="top"
                                                                title="insurance" class="material-icons">favorite
                                                            </i><span>insurance</span>
                                                            @endif
                                                            <br>
                                                            @if($item->getBoat->tax=='on')
                                                            <i data-toggle="tooltip" data-placement="top" title="tax"
                                                                class="material-icons">local_atm
                                                            </i><span>tax</span>
                                                            @endif
                                                            <br>
                                                            <a type="button" class="mt-1 btn-link" data-toggle="modal"
                                                                data-target="#detail{{$item->id}}">
                                                                Detail & policy</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <h3 class="text-center">Price <br>
                                        {{$item->price}}</h3>
                                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                        data-target="#choose{{ $item->id }}">
                                        CHOOSE DEPARTURE
                                    </button>
                                    <!-- Modal -->
                                    <!-- Modal -->
                                    <div class="modal fade" id="choose{{ $item->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span
                                                            aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Your booking cart</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            <h3>Departure</h3>
                                                            <h4>{{ $departure }}</h4>
                                                        </div>
                                                        <div class="col-4">
                                                            <h3>{{ $item->getBoat->title }}</h3>
                                                            <h4>{{ $passanger }}</h4>
                                                        </div>
                                                        <div class="col-2">
                                                            <h3>{{$item->time_departing}}</h3>
                                                            <h4>{{$item->getFrom->name}}</h4>
                                                        </div>
                                                        <div class="col-2">
                                                            <h3>{{$item->time_arrived}}</h3>
                                                            <h4>{{$item->getTo->name}}</h4>
                                                        </div>
                                                        <div class="col-2">
                                                            <h3>Price</h3>
                                                            <h4>{{$item->price}}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{ route('cart-boat', ['id' => $item->id])}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="passanger" value="{{ $passanger }}">
                                                        <input type="hidden" name="departure" value="{{ $departure }}">
                                                        <input type="submit" class="btn btn-primary" value="Book Now">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!--/.single-package-item-->
            </div>
        </div>
        <!--/.packages-content-->
        <div class="mt-5">{!! $result->render() !!}</div>
    </div>
</section>



@include('includeuser.footer')
@include('includeuser.foot')
