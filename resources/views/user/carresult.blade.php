@include('includeuser.head')
@include('includeuser.header')


<section id="home" class="about-us-search">
    <div class="container">
        <div class="about-us-content">
            <div class="row">

            </div>
            <!--/.row-->
        </div>
        <!--/.about-us-content-->
    </div>
    <!--/.container-->

</section>
<!--/.about-us-->
<!--about-us end -->
<!-- lorem -->
<!--travel-box start-->
@include('includeuser.search')

<!--/.travel-box-->
<section>
    <div class="container">
        <div class="mx-3">
            @if(count($result))
            <div class="alert alert-info" role="alert">
                <h4 class="mt-4">
                    Search Result
                </h4>
            </div>
            @else
            <div class="alert alert-info" role="alert">
                <h4 class="mt-4">
                    No Result
                </h4>
            </div>
            @endif
            <div class="row">
                @foreach($result as $item)
                <div class="col-sm-12 mt-2">
                    <div class="single-package-item sticky-top pt-3">
                        <div class="single-package-item-txt">
                            <div class="row">
                                <div class="col-sm-3">
                                    <img width="50%" src="{{ asset('images/car/'.$item->getCar->image)}}" alt="">
                                </div>
                                <div class="col-sm-6">
                                    <h3>{{ $item->getCar->title }}</h3>
                                    @if($item->getCar->include_driver=='on')
                                    <h5 class="mt-2"><i class="material-icons">directions_car</i> Include Driver</h5>
                                    @endif
                                    @if($item->getCar->include_fuel=='on')
                                    <h5><i class="material-icons">local_gas_station</i> Include fuel</h5>
                                    @endif
                                    <h5><i class="material-icons">people</i>{{$item->getCar->max_person}}Person</h5>
                                    <h5><i class="material-icons">business_center</i>{{$item->getCar->max_bag}}Bag</h5>
                                    <div class="dropdown mt-1">
                                        <button class="btn btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            {{$item->getTrip->name}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
                                            <li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
                                            <li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <h3 class="text-center">Price<br>
                                        {{$item->price}}</h3>
                                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
                                        data-target="#choose{{ $item->id }}">
                                        CHOOSE DEPARTURE
                                    </button>
                                    <div class="modal fade" id="choose{{ $item->id }}" tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span
                                                            aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Your Booking</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <img width="50%"
                                                                src="{{ asset('images/car/'.$item->getCar->image)}}"
                                                                alt="">
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h3>{{ $item->getCar->title }}</h3>
                                                            @if($item->getCar->include_driver=='on')
                                                            <h5 class="mt-2"><i
                                                                    class="material-icons">directions_car</i> Include
                                                                Driver</h5>
                                                            @endif
                                                            @if($item->getCar->include_fuel=='on')
                                                            <h5><i class="material-icons">local_gas_station</i> Include
                                                                fuel</h5>
                                                            @endif
                                                            <h5><i
                                                                    class="material-icons">people</i>{{$item->getCar->max_person}}Person
                                                            </h5>
                                                            <h5><i
                                                                    class="material-icons">business_center</i>{{$item->getCar->max_bag}}Bag
                                                            </h5>
                                                            <div class="dropdown mt-1">
                                                                <button class="btn btn-secondary dropdown-toggle"
                                                                    type="button" id="dropdownMenuButton"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false">
                                                                    {{$item->getTrip->name}}
                                                                </button>
                                                                <div class="dropdown-menu"
                                                                    aria-labelledby="dropdownMenuButton">
                                                                    <li><a href="detailtour.php">Half Day Tour West Nusa
                                                                            Penida</a></li>
                                                                    <li><a href="detailtour.php">Half Day Tour West Nusa
                                                                            Penida</a></li>
                                                                    <li><a href="detailtour.php">Half Day Tour West Nusa
                                                                            Penida</a></li>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 mt-5">
                                                                <form action="{{ route('cart-car', ['id' => $item->id])}}" method="post">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label>Pick up location</label>
                                                                        <input type="text" name="pickdetail" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-group">
                                                                        <label>Pick up time(local time)</label>
                                                                        <input type="time" name="picktime" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <label>Request</label>
                                                                        <textarea class="form-control"
                                                                            rows="3" name="pickrequest"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                        @csrf
                                                    <input type="hidden" name="departure" value="{{ $departure }}">
                                                    
                                                    <input type="submit" type="button"
                                                        class="btn btn-primary">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!--/.single-package-item-->
            </div>
        </div>
        <!--/.packages-content-->
        <div class="mt-5">{!! $result->render() !!}</div>

    </div>
</section>
@include('includeuser.footer')
@include('includeuser.foot')
