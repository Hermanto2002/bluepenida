@include('includeuser.head')
@include('includeuser.header')


<section id="home" class="about-us-blog">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 style="color:white; font-weight: bold;">BLOG</h1>
			</div>	
		</div><!--/.row-->
	</div><!--/.container-->
</section><!--/.about-us-->
<nav aria-label="breadcrumb bg-white border">
    <ol class="breadcrumb bg-white container" style="background-color:white;">
        <li class="breadcrumb-item mx-4">
            <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>
<section>
	<div class="container">
		<div class="row px-3">
			<div class="col-md-9">
				<img src="{{ asset('images/blog/'.$blog->image) }}" alt="{{ $blog->title }}">
				<h2 class="mx-2">{{$blog->title}}</h2>
				<p>
					<span class="mr-5">JUL 6, 2019</span><span> 0 COMMENT</span>
				</p>

				<p>{!! $blog->content !!}</p>

				<!-- <div class="card px-3 mt-5" style="border:solid 1px gray; border-radius: 3px;">
					<div class="card-body my-3">
						<div class="row">
							<div class="col-sm-2">
								<div class="text-left">
									<img width="" src="https://in.bmscdn.com/webin/profile/user.jpg" class="rounded img-thumbnail" alt="...">
								</div>
							</div>
							<div class="col-sm-7">
								<td class="align-middle">Nyoman</td>
							</div>
						</div>
					</div>
				</div> -->
<br><br>
				<div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="" data-numposts="5"></div>
			</div>
			<div class="col-md-3">
				<!-- <div class="single-tab-select-box">
					<div class="travel-check-icon travel-search-icon">
						<form action="{{ route('blog-search')}}" method="get">
							<input type="text" name="title" class="form-control" placeholder="Search">
						</form>
					</div>
				</div>-->
				<h4>Blog Category</h4>
				@foreach($categorymenu as $item)
				<a class="btn btn-default mt-1" href="/blog/category/{{$item->id}}" role="button">{{ $item->name }}</a>
				@endforeach
				<h4 class="mt-4">Recent Posts</h4> <br>
				@foreach($newpost as $item)
				<div class="card mb-4">
					<div class="row no-gutters">
						<div class="col-md-5">
							<img src="{{ asset('images/blog/'.$item->image) }}" alt="{{ $item->title }}" class="card-img">
						</div>
						<div class="col-md-7">
							<div class="card-body">
								<h5 class="card-title">{{ $item->title }}</h5>
								<p class="card-text">{!! str_limit($item->content, 100, '...') !!}</p>
								<p class="card-text"><small class="text-muted">{{$item->created_at->diffForHumans()}}</small></p>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				
			</div>
		</div>
	</div>
</section>

@include('includeuser.footer')
@include('includeuser.foot')