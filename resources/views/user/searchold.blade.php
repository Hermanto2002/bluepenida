<form action="{{ route('search') }}" method="get">
	<div class="form-group">
		<label for="exampleFormControlSelect1">sub lokasi</label>
		<select name="location_from_id" class="form-control" id="exampleFormControlSelect1">
			@foreach($data as $item)
				@if($item->parent_id == 0)
					<option value="{{$item->id}}">{{$item->name}}</option>
				@endif
				@foreach($data as $sub)
					@if($sub->parent_id == $item->id)
						<option value="{{$sub->id}}">-- {{$sub->name}}</option>
					@endif
				@endforeach
			@endforeach
		</select>
	</div>
</form>