 @include('includeuser.head')
@include('includeuser.header')

<section id="home" class="about-us-blog">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 style="color:white; font-weight: bold;">BLOG</h1>
			</div>	
		</div><!--/.row-->
	</div><!--/.container-->
</section><!--/.about-us-->
<nav aria-label="breadcrumb bg-white border">
    <ol class="breadcrumb bg-white container" style="background-color:white;">
        <li class="breadcrumb-item mx-4">
            <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>
<section>
	<div class="container">
		<div class="row px-3">
			<div class="col-md-9">
				<div class="row ">
					@foreach($post as $item)

					<div class="col-md-6 col-sm-12">
						<div class="single-package-item">
							<img src="{{ asset('images/blog/'.$item->image) }}" alt="{{ $item->title }}">
							<div class="single-package-item-txt">
								<h3>{{ $item->title }}</h3>
								<div class="packages-para">
									<p>
										<span>{{$item->created_at->diffForHumans()}}</span>
									</p>
									<p>
										{!! str_limit($item->content, 100, '...') !!}
									</p>
								</div><!--/.packages-para-->
								<div class="about-btn">
									<a href="/blog/{{$item->slug}}" class="about-view packages-btn">
										READ MORE
									</a>
								</div><!--/.about-btn-->
							</div><!--/.single-package-item-txt-->
						</div><!--/.single-package-item-->

					</div><!--/.col-->

					@endforeach
				</div>
				<div class="mt-5">{!! $post->render() !!}</div>
			</div>
			<div class="col-md-3">
				<h4>Blog Category</h4>
				@foreach($categorymenu as $item)
				<a class="btn btn-default mt-1" href="/blog/category/{{$item->id}}" role="button">{{ $item->name }}</a>
				@endforeach
				<h4 class="mt-4">Recent Posts</h4> <br>
				@foreach($newpost as $item)
				<div class="card mb-4">
					<div class="row no-gutters">
						<div class="col-md-5">
							<img src="{{ asset('images/blog/'.$item->image) }}" alt="{{ $item->title }}" class="card-img" alt="...">
						</div>
						<div class="col-md-7">
							<div class="card-body">
								<h5 class="card-title">{{ $item->title }}</h5>
								<p class="card-text">{!! str_limit($item->content, 100, '...') !!}</p>
								<p class="card-text"><small class="text-muted">{{$item->created_at->diffForHumans()}}</small></p>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>

@include('includeuser.footer')
@include('includeuser.foot')