@include('includeuser.head')
@include('includeuser.header')
<div class="owl-carousel owl-theme" style="z-index: -1">
@foreach($slider as $item)
<section id="home" class="about-us item" style="background: url('{{ asset('images/page/'.$item->image)}}')no-repeat;">
	<div class="container">
		<div class="about-us-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="single-about-us">
						<div class="about-us-txt">
							<h1 style="color: white;" class="text-center text-white">
								{{$item->text}}
							</h1>
						</div><!--/.about-us-txt-->
					</div><!--/.single-about-us-->
				</div><!--/.col-->
				<div class="col-sm-0">
					<div class="single-about-us">

					</div><!--/.single-about-us-->
				</div><!--/.col-->
			</div><!--/.row-->
		</div><!--/.about-us-content-->
	</div><!--/.container-->
</section><!--/.about-us-->
@endforeach

</div>
<!--about-us end -->

<!--travel-box start-->
@include('includeuser.search')

<!--travel-box end-->


<section class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img src="https://bluepenida.com/wp-content/uploads/2019/07/nusa-penida-3631287_1920-1024x1024.jpg">
			</div>	
			<div class="col-md-8">
				<h1>The beauty of Mount Agung is seen from Nusa Penida</h1>
				<p>
					If you visit Nusa Penida there are many beauties that you can meet.  From beautiful beaches, clear springs and graceful, charming hills.

					Apart from that beauty, from the island of Nusa Penida you can see the beauty of Mount Agung which is beautifully towering.

					There you will see a class of how the Mount Agung is triangular in shape with its peak blunted with the upper part.

					When the weather is sunny, especially in the morning the atmosphere is getting more beautiful with a red tinge of morning passing around the hill.

					Especially lately due to Aging Mountain alert 3, the smoke rising from the summit of the mountain which appeared slowly making Mount Agung besides beautiful also kept a mystery from a distance.

					Some traditional fishermen who use the expanded screen add to the beautiful colors of the sea and Mount Agung from Nusa Penida.

					Have you seen the beauty of Mount Agung from Nusa Penida? Hurry to buy a tour package here.
				</p>
			</div>
		</div>
	</div>
</section>
<!--galley start-->
<section id="gallery" class="gallery">
	<div class="container">
		<div class="gallery-details">
			<div class="gallary-header text-center">
				<h2>
					top destination
				</h2>
				<p>
					Duis aute irure dolor in  velit esse cillum dolore eu fugiat nulla.  
				</p>
			</div><!--/.gallery-header-->
			<div class="gallery-box">
				<div class="gallery-content">
					<div class="filtr-container">
						<div class="row">
							@foreach($trip as $item)
							<div class="col-md-4">
								<div class="filtr-item">
									<img src="https://bluepenida.com/wp-content/uploads/2019/05/diamond-beach-770x375.jpg" alt="portfolio image"/>
									<div class="item-title">
										<a href="#">
											{{ $item->name }}
										</a>
									</div><!-- /.item-title -->
								</div><!-- /.filtr-item -->
							</div><!-- /.col -->
							@endforeach
						</div><!-- /.row -->

					</div><!-- /.filtr-container-->
				</div><!-- /.gallery-content -->
			</div><!--/.galley-box-->
		</div><!--/.gallery-details-->
	</div><!--/.container-->

</section><!--/.gallery-->
<!--gallery end-->


<!--discount-offer start-->

<!--discount-offer end-->

<!--packages start-->
<section id="pack" class="packages">
	<div class="container">
		<div class="gallary-header text-center">
			<h2>
				Tourist Information
			</h2>
		</div><!--/.gallery-header-->
		<div class="packages-content">
			<div class="row">
				@foreach($blog as $item)
				<div class="col-md-4 col-sm-6">
					<div class="single-package-item">
						<img src="{{ asset('image/blog/.$item->image') }}">
						<div class="single-package-item-txt">
							<h3>{{ $item->title }}</h3>
							<div class="packages-para">
								{!! str_limit($item->content, 100, '...') !!}
							</div><!--/.packages-review-->
							<div class="about-btn">
								<a href="/blog/{{$item->slug}}" class="about-view packages-btn">
									READ MORE
								</a>
							</div><!--/.about-btn-->
						</div><!--/.single-package-item-txt-->
					</div><!--/.single-package-item-->

				</div><!--/.col-->
				@endforeach
			</div><!--/.row-->
		</div><!--/.packages-content-->
	</div><!--/.container-->

</section><!--/.packages-->
<!--packages end-->
<section>
	<div class="container">
		<div class="row">

			<div class="col-md-8">
				<h1>“Ledok-Ledok” Is a Delicious Nusa Penida Food</h1>
				<p>
					Padang cuisine is a typical Sumatran food, Padang. Lawar is a Balinese specialty. “Serombotan” is a typical food of Klungkung. That is the typical food of an area that has its own characteristics.

					As for other regions, there are also special foods in Nusa Penida, namely “ledok-ledok”. The plumbing is made of beans, corn which has been pounded with vegetables, cassava which is cooked into one based on the rate of rapid maturity.

					If there is tuna on the team, the plump feels more delicious. Sometimes also given “gerang” or small fish that are dried and fried. The dish makes us look drooling and want to feel it.

					“Ledok-ledok” was originally a staple food of the Nusa Penida community every day. According to explosive research has a complete nutritional content. That is because “ledok-ledok” complete, consist of various types of food ingredients.

					In the midst of hectic tourism, the unique food of Nusa Penida can be a culinary tour. After the tourists are tired of enjoying the beautiful scenery of Nusa Penida, the “ledok-ledok” can be presented to guests to be introduced. Although maybe at first it feels strange and strange for those who are first, but if once or twice will make it addictive.

					You want to make a “ledok-ledok”, you can make it by lodging owned by local Nusa Penida people.
				</p>
			</div>
			<div class="col-md-4">
				<img src="https://bluepenida.com/wp-content/uploads/2019/06/ledok-khas-nusa-penida-1024x1024.jpg">
			</div>	
		</div>
	</div>
</section>

<!-- testemonial Start -->
<section   class="testemonial">
	<div class="container">

		<div class="gallary-header text-center">
			<h2>
				Our happy clients
			</h2>
			<p>
				Duis aute irure dolor in  velit esse cillum dolore eu fugiat nulla. 
			</p>

		</div><!--/.gallery-header-->

		<div class="owl-carousel owl-theme" id="testemonial-carousel">

			<div class="home1-testm item">
				<div class="home1-testm-single text-center">
					<div class="home1-testm-img">
						<img src="https://bluepenida.com/wp-content/uploads/2018/12/default_avatar-70x70.png" alt="img"/>
					</div><!--/.home1-testm-img-->
					<div class="home1-testm-txt">
						<span class="icon section-icon">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</span>
						<p>
							Lorem ipsum dolor sit amet, contur adip elit, sed do mod incid ut labore et dolore magna aliqua. Ut enim ad minim veniam. 
						</p>
						<h3>
							<a href="#">
								kevin watson
							</a>
						</h3>
						<h4>london, england</h4>
					</div><!--/.home1-testm-txt-->	
				</div><!--/.home1-testm-single-->

			</div><!--/.item-->

			<div class="home1-testm item">
				<div class="home1-testm-single text-center">
					<div class="home1-testm-img">
						<img src="https://bluepenida.com/wp-content/uploads/2018/12/default_avatar-70x70.png" alt="img"/>
					</div><!--/.home1-testm-img-->
					<div class="home1-testm-txt">
						<span class="icon section-icon">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</span>
						<p>
							Lorem ipsum dolor sit amet, contur adip elit, sed do mod incid ut labore et dolore magna aliqua. Ut enim ad minim veniam. 
						</p>
						<h3>
							<a href="#">
								kevin watson
							</a>
						</h3>
						<h4>london, england</h4>
					</div><!--/.home1-testm-txt-->	
				</div><!--/.home1-testm-single-->

			</div><!--/.item-->
			<div class="home1-testm item">
				<div class="home1-testm-single text-center">
					<div class="home1-testm-img">
						<img src="https://bluepenida.com/wp-content/uploads/2018/12/default_avatar-70x70.png" alt="img"/>
					</div><!--/.home1-testm-img-->
					<div class="home1-testm-txt">
						<span class="icon section-icon">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</span>
						<p>
							Lorem ipsum dolor sit amet, contur adip elit, sed do mod incid ut labore et dolore magna aliqua. Ut enim ad minim veniam. 
						</p>
						<h3>
							<a href="#">
								kevin watson
							</a>
						</h3>
						<h4>london, england</h4>
					</div><!--/.home1-testm-txt-->	
				</div><!--/.home1-testm-single-->

			</div><!--/.item-->
			<div class="home1-testm item">
				<div class="home1-testm-single text-center">
					<div class="home1-testm-img">
						<img src="https://bluepenida.com/wp-content/uploads/2018/12/default_avatar-70x70.png" alt="img"/>
					</div><!--/.home1-testm-img-->
					<div class="home1-testm-txt">
						<span class="icon section-icon">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</span>
						<p>
							Lorem ipsum dolor sit amet, contur adip elit, sed do mod incid ut labore et dolore magna aliqua. Ut enim ad minim veniam. 
						</p>
						<h3>
							<a href="#">
								kevin watson
							</a>
						</h3>
						<h4>london, england</h4>
					</div><!--/.home1-testm-txt-->	
				</div><!--/.home1-testm-single-->

			</div><!--/.item-->
			<div class="home1-testm item">
				<div class="home1-testm-single text-center">
					<div class="home1-testm-img">
						<img src="https://bluepenida.com/wp-content/uploads/2018/12/default_avatar-70x70.png" alt="img"/>
					</div><!--/.home1-testm-img-->
					<div class="home1-testm-txt">
						<span class="icon section-icon">
							<i class="fa fa-quote-left" aria-hidden="true"></i>
						</span>
						<p>
							Lorem ipsum dolor sit amet, contur adip elit, sed do mod incid ut labore et dolore magna aliqua. Ut enim ad minim veniam. 
						</p>
						<h3>
							<a href="#">
								kevin watson
							</a>
						</h3>
						<h4>london, england</h4>
					</div><!--/.home1-testm-txt-->	
				</div><!--/.home1-testm-single-->

			</div><!--/.item-->

		</div><!--/.testemonial-carousel-->
	</div><!--/.container-->

</section><!--/.testimonial-->	
<!-- testemonial End -->


<!--blog start-->
<section id="pack" class="packages">
	<div class="container">
		<div class="gallary-header text-center">
			<h2>
				Reasons To Book With Us
			</h2>
			<p>
				Duis aute irure dolor in  velit esse cillum dolore eu fugiat nulla.  
			</p>
		</div><!--/.gallery-header-->
		<div class="packages-content">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<h4 class="font-weight-bold">Rescheduling and Easy Refunds</h4>
					<br>
					<p>
						Don’t worry about ordering tickets online with us, we support easy rescheduling and refunds for your travel flexibility. We guarantee 100% Money Back. With the condition that you have successfully made a payment, our system automatically sends an electronic ticket to your email as proof.
					</p>

				</div><!--/.col-->
				<div class="col-md-4 col-sm-6">
					<h4 class="font-weight-bold">Rescheduling and Easy Refunds</h4>
					<br>
					<p>
						Don’t worry about ordering tickets online with us, we support easy rescheduling and refunds for your travel flexibility. We guarantee 100% Money Back. With the condition that you have successfully made a payment, our system automatically sends an electronic ticket to your email as proof.
					</p>

				</div><!--/.col-->
				<div class="col-md-4 col-sm-6">
					<h4 class="font-weight-bold">Rescheduling and Easy Refunds</h4>
					<br>
					<p>
						Don’t worry about ordering tickets online with us, we support easy rescheduling and refunds for your travel flexibility. We guarantee 100% Money Back. With the condition that you have successfully made a payment, our system automatically sends an electronic ticket to your email as proof.
					</p>

				</div><!--/.col-->

			</div><!--/.row-->
		</div><!--/.packages-content-->
	</div><!--/.container-->

</section><!--/.packages-->
<!--blog end-->

@include('includeuser.footer')
@include('includeuser.foot')