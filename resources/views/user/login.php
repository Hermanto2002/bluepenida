<?php include'head.php' ?>

<section>
    <div class="container">
        <div class="offset-md-3 col-md-6" style="margin-top:70px;">
            <div class="single-package-item">
                <div class="single-package-item-txt">
                    <h3>LOGIN</h3>
                    <!--/.packages-para-->
                    <form class="mx-5">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1"
                                placeholder="Password">
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-block">Login</button>
                    </form>
                </div>
                <div class="single-package-item-txt">
                    <p class="text-center mb-1">or continue with</p>
                    <p class="text-center">
                        <button type="button" class="btn btn-secondary">Facebook</button>
                        <button type="button" class="btn btn-secondary">Google</button>
                        <button type="button" class="btn btn-secondary">Twitter</button>
                    </p>
                    <p class="text-center mt-3">Do not have an account? <a href="">Sign Up</a></p>
                </div>
                <!--/.single-package-item-txt-->
            </div>
            <!--/.single-package-item-->

        </div>
    </div>
    <!--/.container-->

</section>
<!--/.gallery-->

<?php include'foot.php' ?>