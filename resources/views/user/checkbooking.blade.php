@include('includeuser.head')
@include('includeuser.header')

<section id="home" class="about-us-search">
	<div class="container">
		<div class="about-us-content">
			<div class="row">

			</div>
			<!--/.row-->
		</div>
		<!--/.about-us-content-->
	</div>
	<!--/.container-->

</section>
<section  class="travel-box" style="z-index: 2;">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="single-travel-boxes">
					<div id="desc-tabs" class="desc-tabs">
						<!-- Tab panes -->
						<h1 class="mt-2 ml-1">Check Booking</h1>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active fade in" id="tours">
								<div class="tab-para">
									<form action="{{ route('checkbookingresult') }}" method="post">
										@csrf
										<div class="row">
											<div class="col-sm-4">
												<div class="single-tab-select-box">
													<h2>Order Code</h2>
													<input type="text" name="code_booking" class="form-control">
												</div>
												<!--/.single-tab-select-box-->
											</div>
											<div class="col-sm-4">
												<div class="single-tab-select-box">
													<h2>Email</h2>
													<input type="email" name="email" class="form-control">
												</div>
												<!--/.single-tab-select-box-->
											</div>											
											<div class="col-sm-4">
												<div class="about-btn pull-right">
													<button  class="about-view travel-btn">
														search	
													</button><!--/.travel-btn-->
												</div><!--/.about-btn-->
											</div><!--/.col-->
										</div><!--/.row-->
									</form>
								</div><!--/.tab-para-->
							</div><!--/.tabpannel-->
						</div><!--/.tab content-->
					</div><!--/.desc-tabs-->
				</div><!--/.single-travel-box-->
			</div><!--/.col-->
		</div><!--/.row-->
	</div><!--/.container-->
</section><!--/.travel-box-->

@include('includeuser.footer')
@include('includeuser.foot')