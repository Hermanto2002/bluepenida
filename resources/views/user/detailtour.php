<?php include'header.php' ?>

<nav aria-label="breadcrumb bg-white border">
    <ol class="breadcrumb bg-white container" style="background-color:white;">
        <li class="breadcrumb-item mx-4">
            <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>
<div class="container">
    <div class="row">
        <div class="col-sm-10">
            <h2>Watersport Package A</h2>
            <p>Angel Billabong Nusa Penida</p>
        </div>
        <div class="col-sm-2">
            <p class="text-center">Not Rate</p>
            <div id="rateYo" style="z-index:-1;"></div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-3 col-xs-3 mx-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <svg width="32px" height="32px" viewBox="0 0 34 34" version="1.1"
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->

                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <g id="Tour_Detail_1" transform="translate(-134.000000, -1005.000000)"
                                                stroke="#5191FA">
                                                <g id="tour-detail" transform="translate(0.000000, 211.000000)">
                                                    <g id="feauture" transform="translate(135.000000, 765.000000)">
                                                        <g id="Group-3">
                                                            <g id="Group" transform="translate(0.000000, 25.000000)">
                                                                <g id="ico_clock"
                                                                    transform="translate(0.000000, 5.000000)">
                                                                    <circle id="Oval" cx="16" cy="16" r="16"></circle>
                                                                    <circle id="Oval" cx="16" cy="17.3333333"
                                                                        r="2.28571429">
                                                                    </circle>
                                                                    <path d="M16,15.047619 L16,7.04761905" id="Shape">
                                                                    </path>
                                                                    <path
                                                                        d="M17.6167619,18.9500952 L21.7142857,23.047619"
                                                                        id="Shape"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-sm-8">
                                    <h5>Duration</h5>
                                    <small>7 Hours</small>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 mx-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <svg width="32px" height="32px" viewBox="0 0 34 34" version="1.1"
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->

                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <g id="Tour_Detail_1" transform="translate(-335.000000, -1005.000000)"
                                                stroke="#5191FA">
                                                <g id="tour-detail" transform="translate(0.000000, 211.000000)">
                                                    <g id="feauture" transform="translate(135.000000, 765.000000)">
                                                        <g id="Group" transform="translate(201.000000, 25.000000)">
                                                            <g id="ico_tour_type"
                                                                transform="translate(0.000000, 5.000000)">
                                                                <path
                                                                    d="M13.4810667,23.7283556 C13.7772954,25.6164963 13.043711,27.5192464 11.5566476,28.7198584 C10.0695841,29.9204704 8.05496197,30.2365427 6.27166976,29.549014 C4.48837756,28.8614852 3.20733983,27.2748074 2.91111111,25.3866667 L2.74613333,24.3299556 C2.65661545,23.7692095 2.79434998,23.1959129 3.12883306,22.7370331 C3.46331614,22.2781533 3.96694361,21.9715605 4.52817778,21.8851556 L10.8712889,20.8896 C11.432035,20.8000821 12.0053315,20.9378166 12.4642113,21.2722997 C12.9230912,21.6067828 13.2296839,22.1104103 13.3160889,22.6716444 L13.4810667,23.7283556 Z"
                                                                    id="Shape"></path>
                                                                <path
                                                                    d="M5.11555556,0.1408 C1.71134814,0.982901045 -0.457506858,4.31950538 0.154844444,7.77244444 L1.41777778,15.8691556 C1.60071,17.0365699 2.69505968,17.8348913 3.86257778,17.6526222 L10.2042667,16.6570667 C10.7657612,16.571 11.2697435,16.2645497 11.6045222,15.8056305 C11.9393009,15.3467113 12.0772269,14.7732252 11.9877333,14.2122667 L11.9592889,14.0088889 C11.7659953,12.7663569 11.9436025,11.4944126 12.4698667,10.3523556 C12.9981751,9.20988894 13.1758566,7.93642212 12.9804444,6.69297778 L12.7827556,5.4272 C12.5097684,3.68767687 11.5342848,2.1366314 10.0846439,1.13712946 C8.63500309,0.137627517 6.8384634,-0.222588541 5.11555556,0.1408 Z"
                                                                    id="Shape"></path>
                                                                <path
                                                                    d="M18.2583111,25.6995556 C17.8428835,28.5534868 19.7641913,31.223136 22.6023163,31.7355326 C25.4404413,32.2479292 28.1739846,30.4186694 28.7827556,27.5996444 L28.9719111,26.5457778 C29.1807578,25.3831626 28.4089448,24.2709374 27.2467556,24.0597333 L20.9292444,22.9219556 C20.3703881,22.8193233 19.7936791,22.9438755 19.3269888,23.2679956 C18.8602984,23.5921158 18.5421913,24.0890206 18.4432,24.6485333 L18.2583111,25.6995556 Z"
                                                                    id="Shape"></path>
                                                                <path
                                                                    d="M27.1770667,2.31253333 C30.5568647,3.23734789 32.6432678,6.62146013 31.9514667,10.0565333 L30.4965333,18.1233778 C30.3971763,18.68204 30.079348,19.1781018 29.6133417,19.5018508 C29.1473353,19.8255998 28.5715511,19.9503581 28.0133333,19.8485333 L21.6958222,18.7107556 C21.1363095,18.6117642 20.6394047,18.2936571 20.3152845,17.8269668 C19.9911643,17.3602764 19.8666122,16.7835675 19.9692444,16.2247111 L20.0062222,16.0170667 C20.2290597,14.7795819 20.0806008,13.5038248 19.5795556,12.3505778 C19.0784767,11.1968384 18.9300215,9.92061854 19.1528889,8.68266667 L19.3804444,7.42257778 C19.6924113,5.68717624 20.7045511,4.15653375 22.1792495,3.18999024 C23.6539479,2.22344673 25.4613226,1.90612953 27.1770667,2.31253333 Z"
                                                                    id="Shape"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-sm-8">
                                    <h5>Tour Type</h5>
                                    <small>Specific Tour</small>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 mx-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1"
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->

                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Hotel-layout" stroke="none" stroke-width="1" fill="none"
                                            fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                            <g id="Room_Detail_1" transform="translate(-544.000000, -803.000000)"
                                                stroke="#5191FA">
                                                <g id="room-detail" transform="translate(0.000000, 211.000000)">
                                                    <g id="Group-3" transform="translate(135.000000, 562.000000)">
                                                        <g id="Group" transform="translate(409.000000, 30.000000)">
                                                            <g id="ico_adults">
                                                                <g id="Group" transform="translate(1.000000, 1.000000)">
                                                                    <g id="Regular">
                                                                        <circle id="Oval" cx="7" cy="4" r="4"></circle>
                                                                        <path
                                                                            d="M14,17 C14,13.1340068 10.8659932,10 7,10 C3.13400675,10 4.4408921e-16,13.1340068 0,17 L0,20 L3,20 L4,30 L10,30 L11,20 L14,20 L14,17 Z"
                                                                            id="Shape"></path>
                                                                        <path
                                                                            d="M16,24 L18,24 L19,30 L25,30 L26,24 L30,24 L27,15 C26,12 24.7613333,10 22,10 C20.1015957,10.0018584 18.4126862,11.2059289 17.792,13"
                                                                            id="Shape"></path>
                                                                        <circle id="Oval" cx="22" cy="4" r="4"></circle>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-sm-8">
                                    <h5>Group Size</h5>
                                    <small>Unlimited</small>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 mx-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <svg width="32px" height="32px" viewBox="0 0 32 32" version="1.1"
                                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->

                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <g id="Tour_Detail_1" transform="translate(-735.000000, -1005.000000)"
                                                stroke="#5191FA">
                                                <g id="tour-detail" transform="translate(0.000000, 211.000000)">
                                                    <g id="feauture" transform="translate(135.000000, 765.000000)">
                                                        <g id="Group" transform="translate(601.000000, 25.000000)">
                                                            <g transform="translate(0.000000, 5.000000)" id="Regular">
                                                                <g>
                                                                    <circle id="Oval" cx="9" cy="5.5" r="5.5"></circle>
                                                                    <path
                                                                        d="M10,13.0773333 C9.66846827,13.0319989 9.33455994,13.0061766 9,13 C4.02943725,13 5.92118946e-16,17.0294373 0,22"
                                                                        id="Shape"></path>
                                                                    <path
                                                                        d="M28,26 L24,26 L18,30 L18,26 L16,26 C14.8954305,26 14,25.1045695 14,24 L14,16 C14,14.8954305 14.8954305,14 16,14 L28,14 C29.1045695,14 30,14.8954305 30,16 L30,24 C30,25.1045695 29.1045695,26 28,26 Z"
                                                                        id="Shape"></path>
                                                                    <path d="M18,18 L26,18" id="Shape"></path>
                                                                    <path d="M18,22 L26,22" id="Shape"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="col-sm-8">
                                    <h5>Languages</h5>
                                    <small></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-5">
                    <div class="col-xs-11">
                        <div class="fotorama" data-nav="thumbs" data-autoplay="true">
                            <a href=""><img
                                    src="https://bluepenida.com/wp-content/uploads/2019/05/IMG20190508144423-870x555.jpg"
                                    height="96"></a>
                            <a href=""><img
                                    src="https://bluepenida.com/wp-content/uploads/2019/05/IMG20190508144423-870x555.jpg"
                                    height="96"></a>
                            <a href=""><img
                                    src="https://bluepenida.com/wp-content/uploads/2019/05/IMG20190508144423-870x555.jpg"
                                    height="96"></a>
                            <a href=""><img
                                    src="https://bluepenida.com/wp-content/uploads/2019/05/IMG20190508144423-870x555.jpg"
                                    height="96"></a>
                            <a href=""><img
                                    src="https://bluepenida.com/wp-content/uploads/2019/05/IMG20190508144423-870x555.jpg"
                                    height="96"></a>
                            <a href=""><img
                                    src="https://bluepenida.com/wp-content/uploads/2019/05/IMG20190508144423-870x555.jpg"
                                    height="96"></a>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-3 mb-3">
                        <h2>Overview</h2>
                        <h3 class="mt-2 mb-1">Watersport Package A</h3>
                        <p>Water spot is a favourite adventure tour for young travellers. In Nusa Penida the average
                            tourist
                            is still young who likes adventure. If you like it, of course it’s right when you try the
                            water
                            spot Package that we offer.
                            This tour will boost your adrenaline because in the vast ocean you will be invited to play
                            canoe, snorkel, Banana Boat and donut boat. Playing canoe in the waters of Nusa Penida will
                            certainly be unforgettable memories at Nusa Penida especially with your close friends.</p>
                    </div>
                    <hr>
                    <div class="col-sm-12 mt-3 mb-3">
                        <h2>Itinerary</h2>
                        <div class="card px-4 pl-4"
                            style="border:1px solid white; border-radius:5px; background-color:#f7f7f9;">
                            <div class="card-header">
                                <h4>Activities Journey</h4>
                                <br>
                            </div>
                            <div class="card-body">
                                <div class="body">
                                    <p>06.00 - Shutle will pick up you at the hotel lobby<br>
                                        07.30- check in get your activities ticket by showing your voucher<br>
                                        08.00 - Fast Boat depart to Lembongan<br>
                                        08.30- island tour lembongan area<br>
                                        10.30-back to lembongan harbour and depart to Nusa Penida (Semayaone Beach
                                        Club/Nusa
                                        Penida Island)<br>
                                        11.00- Arrival,wellcome drink and having lunch.show your drink voucher get 2
                                        glass
                                        of soft drink<br>
                                        12.00 - watersport activities, <br>
                                        14.30 - Depart to serangan harboaur<br>
                                        15.30- Arrival at serangan, our driver ready to drop you to your hotel.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-3 mb-3">
                        <h2 class="mb-3">Included/Exclude</h2>
                        <div class="row">
                            <div class="col-sm-6">
                                <ul>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                    <li>Free pick up & Drop is offered from/to many south by location such as Kuta,
                                        Legian,
                                        Jimbaran, Ubud Central, Sanur, Nusadua.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 mt-3 mb-3">
                        <h2 class="mb-3">Reviews</h2>
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Excellent</p>
                                <div class="progress">

                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                        aria-valuemax="100"></div>
                                </div>
                                <p>Very Good</p>
                                <div class="progress">

                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p>Average</p>
                                <div class="progress">

                                    <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p>Poor</p>
                                <div class="progress">

                                    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p>Terrible</p>
                                <div class="progress">

                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p>
                                    <button class="btn btn-primary" type="button" data-toggle="collapse"
                                        data-target="#collapseExample" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        Write Review
                                    </button>
                                </p>
                                <div class="collapse mt-4" id="collapseExample">
                                    <div class="card card-body">
                                        <div class="col-sm-6 mb-4">
                                            <label for="formGroupExampleInput">First name *</label>
                                            <input type="text" class="form-control" placeholder="First name">
                                        </div>
                                        <div class="col-sm-6 mb-4">
                                            <label for="formGroupExampleInput">Email *</label>
                                            <input type="email" class="form-control" placeholder="Last name">
                                        </div>
                                        <div class="col-sm-12 mb-4">
                                            <label for="formGroupExampleInput">Title *</label>
                                            <input type="text" class="form-control" placeholder="First name">
                                        </div>
                                        <div class="col-sm-12 mb-4">
                                            <label for="formGroupExampleInput">Content *</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1"
                                                rows="3"></textarea>
                                        </div>
                                        <button class="btn btn-primary" type="button">
                                            Send
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 ">
            <div class="single-package-item sticky-top">
                <div class="single-package-item-txt">
                    <h3>From<span class="pull-right">$499</span></h3>
                    <div class="packages-para">
                        <div class="single-tab-select-box">
                            <h4>Date</h4>
                            <div class="travel-check-icon">
                                <form action="#">
                                    <input type="text" name="check_in" class="form-control" data-toggle="datepicker"
                                        placeholder="12 -01 - 2017 ">
                                </form>
                            </div><!-- /.travel-check-icon -->
                        </div>
                        <div class="single-tab-select-box mt-3">
                            <h4>Adults</h4><small>Age 18+</small>
                            <div class="">
                                <form action="#">
                                    <input type="number" name="check_in" class="form-control">
                                </form>
                            </div><!-- /. -->
                        </div>
                        <div class="single-tab-select-box mt-3">
                            <h4>Children</h4><small>Age 6-17</small>
                            <div class="">
                                <form action="#">
                                    <input type="number" name="check_in" class="form-control">
                                </form>
                            </div><!-- /. -->
                        </div>
                        <div class="single-tab-select-box mt-3">
                            <h4>Infant</h4><small>Age 0-5</small>
                            <div class="">
                                <form action="#">
                                    <input type="number" name="check_in" class="form-control">
                                </form>
                            </div><!-- /.travel-check-icon -->
                        </div>
                        <!--/.single-tab-select-box-->
                    </div>
                    <div class="about-btn">
                        <a href="cart.php">
                        <button class="about-view packages-btn">
                            book now
                        </button>
                    </a>
                    </div>
                    <!--/.about-btn-->
                </div>
                <!--/.single-package-item-txt-->
            </div>
            <!--/.single-package-item-->
        </div>
    </div>
    <section id="pack" class="packages">
        <div class="container">
            <div class="gallary-header text-center">
                <h2>
                    You might also like
                </h2>
            </div>
            <!--/.gallery-header-->
            <div class="packages-content">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="single-package-item">
                            <img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg"
                                alt="package-place">
                            <div class="single-package-item-txt">
                                <h3>Nusa Penida<span class="pull-right">$499</span></h3>
                                <div class="packages-para">
                                    <p>
                                        <span>
                                            <i class="fa fa-angle-right"></i> 5 daays 6 nights
                                        </span>
                                        <i class="fa fa-angle-right"></i> 5 star accomodation
                                    </p>
                                    <p>
                                        <span>
                                            <i class="fa fa-angle-right"></i> transportation
                                        </span>
                                        <i class="fa fa-angle-right"></i> food facilities
                                    </p>
                                </div>
                                <!--/.packages-para-->
                                <div class="packages-review">
                                    <p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>2544 review</span>
                                    </p>
                                </div>
                                <!--/.packages-review-->
                                <div class="about-btn">
                                    <button class="about-view packages-btn">
                                        book now
                                    </button>
                                </div>
                                <!--/.about-btn-->
                            </div>
                            <!--/.single-package-item-txt-->
                        </div>
                        <!--/.single-package-item-->

                    </div>
                    <!--/.col-->

                    <div class="col-md-4 col-sm-6">
                        <div class="single-package-item">
                            <img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg"
                                alt="package-place">
                            <div class="single-package-item-txt">
                                <h3>Nusa Penida<span class="pull-right">$499</span></h3>
                                <div class="packages-para">
                                    <p>
                                        <span>
                                            <i class="fa fa-angle-right"></i> 5 daays 6 nights
                                        </span>
                                        <i class="fa fa-angle-right"></i> 5 star accomodation
                                    </p>
                                    <p>
                                        <span>
                                            <i class="fa fa-angle-right"></i> transportation
                                        </span>
                                        <i class="fa fa-angle-right"></i> food facilities
                                    </p>
                                </div>
                                <!--/.packages-para-->
                                <div class="packages-review">
                                    <p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>2544 review</span>
                                    </p>
                                </div>
                                <!--/.packages-review-->
                                <div class="about-btn">
                                    <button class="about-view packages-btn">
                                        book now
                                    </button>
                                </div>
                                <!--/.about-btn-->
                            </div>
                            <!--/.single-package-item-txt-->
                        </div>
                        <!--/.single-package-item-->

                    </div>
                    <!--/.col-->
                    <div class="col-md-4 col-sm-6">
                        <div class="single-package-item">
                            <img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg"
                                alt="package-place">
                            <div class="single-package-item-txt">
                                <h3>Nusa Penida<span class="pull-right">$499</span></h3>
                                <div class="packages-para">
                                    <p>
                                        <span>
                                            <i class="fa fa-angle-right"></i> 5 daays 6 nights
                                        </span>
                                        <i class="fa fa-angle-right"></i> 5 star accomodation
                                    </p>
                                    <p>
                                        <span>
                                            <i class="fa fa-angle-right"></i> transportation
                                        </span>
                                        <i class="fa fa-angle-right"></i> food facilities
                                    </p>
                                </div>
                                <!--/.packages-para-->
                                <div class="packages-review">
                                    <p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <span>2544 review</span>
                                    </p>
                                </div>
                                <!--/.packages-review-->
                                <div class="about-btn">
                                    <button class="about-view packages-btn">
                                        book now
                                    </button>
                                </div>
                                <!--/.about-btn-->
                            </div>
                            <!--/.single-package-item-txt-->
                        </div>
                        <!--/.single-package-item-->

                    </div>
                    <!--/.col-->

                </div>
                <!--/.row-->
            </div>
            <!--/.packages-content-->
        </div>
        <!--/.container-->

    </section>
</div>
<?php include'footer.php' ?>