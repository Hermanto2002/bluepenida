@include('includeuser.head')
@include('includeuser.header')

<script
src="https://www.paypal.com/sdk/js?client-id=AV4KDUX4Qo0MwVIjq8OpFmhHJcRFTmcnBozwpQ2tNf-kz79zg0P1xtlPs7ES5Z112dUUb7EVvodGQA-B"></script>
<div class="container">
    <form action="{{ route('cart-save')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="row mt-3">
                    <div class="col-sm-12">
                        <h2 class="mb-1 mt-1">Your Booking</h2>
                        @if($car !=null)
                        <div class="single-package-item">
                            <div class="single-package-item-txt">
                                <div class="row">
                                    <div class="col-10">
                                        <h3> {{ $car['name'] }} </h3>
                                    </div>
                                    <div class="col-2">
                                        <a type="button" href="{{ url('cart/car/delete/'.$car['id']) }}"
                                        class="close mt-3" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </a>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 class="mt-3">Departure : {{ $car['departure']}}</h4>
                                            <h4 class="mt-3">Pickup from: {{ $car['pickup_from']}}</h4>
                                            <h4 class="mt-3">Detail Pickup Location: {{ $car['pickdetail']}}</h4>
                                            <h4 class="mt-3">Pickup Time : {{ $car['picktime']}}</h4>
                                            <h4 class="mt-3">Pickip Request : {{ $car['pickrequest']}}</h4>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--/.single-package-item-txt-->
                        <!--/.single-package-item-txt-->
                    </div>
                    <input type="hidden" name="carid" value="{{ $car['id'] }}">
                    @endif
                    <!-- <input type="hidden" name="carid" value="null"> -->

                    @if($boat !=null)
                    <input type="hidden" name="boatid" value="{{ $boat['id'] }}">
                    <div class="single-package-item">
                        <div class="single-package-item-txt">
                            <div class="row">
                                <div class="col-10">
                                    <h3> {{ $boat['name'] }} </h3>
                                </div>
                                <div class="col-2">
                                    <a type="button" href="{{ url('cart/boat/delete/'.$boat['id']) }}"
                                    class="close mt-3" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </a>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">

                                        <h4 class="mt-3">Departure : {{ $boat['departure']}}</h4>
                                        <h3 class="mt-1">{{ $boat['name']}} | {{ $boat['passanger']}} person
                                        </h3>

                                    </div>
                                    <div class="col-4 mt-2">
                                        <h4>{{ $boat['time_departing'] }}</h4>
                                        <h5>{{ $boat['from_id'] }}</h5>
                                    </div>
                                    <div class="col-4 mt-2">
                                        <h4>{{ $boat['time_arrived'] }}</h4>
                                        <h5>{{ $boat['to_id'] }}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--/.single-package-item-txt-->
                </div>
                @endif
                <!--/.single-package-item-->
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="row mt-3">
            <div class="col-sm-12">
                <h2 class="mb-1 mt-1">Summary </h2>
                <div class="single-package-item">
                    @if($boat !=null)
                    <div class="single-package-item-txt">
                        <div class="container">
                            <h5 class="mt-4">Boat Departure</h5>
                            <div class="row" class="mt-2">
                                <div class="col-6">
                                    <h4>{{ $boat['name'] }}</h4>
                                </div>
                                <div class="col-6">
                                    <h4>Rp. {{ $priceboat }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="priceboat" value="{{ $boat['price'] }}">
                    @endif
                    @if($car !=null)
                    <div class="single-package-item-txt">
                        <div class="container">
                            <h5 class="mt-4">Car Departure</h5>
                            <div class="row" class="mt-2">
                                <div class="col-6">
                                    <h4>{{ $car['name'] }}</h4>
                                </div>
                                <div class="col-6">
                                    <h4>Rp. {{ $car['price'] }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="pricecar" value="{{ $car['price'] }}">
                    @endif
                    <hr>
                    <div class="single-package-item-txt">
                        <div class="container">
                            <div class="row" class="mt-2">
                                <div class="col-6">
                                    <h4>Total</h4>
                                </div>
                                <div class="col-6">
                                    <h4>Rp. {{ $total }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <h4 class="mb-1 mt-1"> Fill your information below</h4>
                <div class="single-package-item">
                    <div class="single-package-item-txt">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mt-3">
                                        <label>Name</label>
                                        <input type="text" name="firsname" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="number" name="phone" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control">
                                    </div>
                                </div>
                                
                                <button  type="submit" class="btn btn-primary btn-block">Continue
                                Payment</button>
                            </div>

                        </div>
                    </div>
                    <!--/.single-package-item-txt-->
                </div>
                <!--/.single-package-item-->
            </div>
        </div>
    </div>
</div>
</form>
</div>

<script>
    paypal.Buttons({
      createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: '0.01'
        }
    }]
});
  }
}).render('#paypal-button-container');
</script>
@include('includeuser.footer')
@include('includeuser.foot')
