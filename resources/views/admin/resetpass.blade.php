@include('includeadmin.header')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    @include('includeadmin.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('includeadmin.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Post trip</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Post trip form</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           <form action="{{ route('admin.data.update') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id }}">
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group">
                  <label>Name</label>
                  <input name="name" type="text" class="form-control" value="{{$data->name}}">
                </div>
                <div class="form-group">
                  <label>Password (fill to change)</label>
                  <input name="password" type="text" class="form-control">
                </div>
              </div>
            </div>
            <div style="text-align: right;">
              <button class="btn btn-success">Post</button>
            </div>
          </form>
          @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
        </div>
        <!-- /.card-body -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- footer -->
  @include('includeadmin.footer')
  <!-- end footer -->
</div>
<!-- ./wrapper -->
@include('includeadmin.foot')