@include('includeadmin.header')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    @include('includeadmin.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('includeadmin.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Post locations</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Post locations form</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           @if($form_mode == 'add')
           <form action="{{ route('admin.localtions.store') }}" method="post"
           enctype="multipart/form-data">
           @else
           <form action="{{ route('admin.localtions.update') }}" method="post">
            <input type="hidden" name="id" value="{{ @$data->id }}">
            @endif
            @csrf
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group">
                  <label>Name</label>
                  <input name="name" type="text" class="form-control" value="{{ @$data->name }}">
                </div>
                <div class="form-group form-check">
                  <input type="checkbox" class="form-check-input" id="_My.notFinal">
                  <label class="form-check-label" for="exampleCheck1">Apakah ini sub lokasi?</label>
                </div>
                <div id="LicenseCustomer" style="display:none">
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">sub lokasi</label>
                    <select name="parent_id" class="form-control" id="exampleFormControlSelect1">
                      <option value="0" selected >-</option>
                      @foreach($localtions as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                
              </div>
            </div>
            <div style="text-align: right;">
              <button class="btn btn-success">Post</button>
            </div>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- footer -->
  @include('includeadmin.footer')
  <!-- end footer -->
</div>
<!-- ./wrapper -->

@include('includeadmin.foot')