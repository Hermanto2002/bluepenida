@include('includeadmin.header')

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        @include('includeadmin.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('includeadmin.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Projects</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Projects</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Projects</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                                title="Remove">
                                <i class="fas fa-times"></i></button>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputName">Booking Name</label>
                                <input type="text" id="inputName" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select class="form-control custom-select">
                                    <option selected="" disabled="">Select one</option>
                                    <option>On Hold</option>
                                    <option>Canceled</option>
                                    <option selected="">Success</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputClientCompany">Customer Name</label>
                                <input type="text" id="inputClientCompany" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label for="inputClientCompany">Customer Phone</label>
                                <input type="text" id="inputClientCompany" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label for="inputClientCompany">Customer Email</label>
                                <input type="text" id="inputClientCompany" class="form-control" value="">
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- footer -->
        @include('includeadmin.footer')
        <!-- end footer -->
    </div>
    <!-- ./wrapper -->
    @include('includeadmin.foot')
