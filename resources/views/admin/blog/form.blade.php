@include('includeadmin.header')

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        @include('includeadmin.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('includeadmin.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Create Blog</h1>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Create Blog Form</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @if($form_mode == 'add')
                        <form action="{{ route('admin.blog.store') }}" method="post" enctype="multipart/form-data">
                            @else
                            <form action="{{ route('admin.blog.update') }}" method="post">
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input name="title" type="text" class="form-control"
                                            value="{{ @$data->title }}">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Category</label>
                                            <select name="category_id" class="form-control"
                                            id="exampleFormControlSelect1">
                                            @foreach($category as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <label>Content</label>
                                    <textarea>{{ @$data->content }}</textarea>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                      <label for="exampleFormControlFile1">Image</label>
                                      <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1" value="{{ @$data->image }">
                                  </div>
                                  <input type="hidden" name="type" value="Boat">
                              </div>
                          </div>

                          <div style="text-align: right;">
                            <button class="btn btn-success">Post</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- footer -->
    @include('includeadmin.footer')
    <!-- end footer -->
</div>
<!-- ./wrapper -->
@include('includeadmin.foot')
