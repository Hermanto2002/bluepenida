@include('includeadmin.header')

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        @include('includeadmin.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('includeadmin.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Post trip</h1>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            To Do List
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        @if($form_mode == 'add')
                        <form action="{{ route('admin.slider.store') }}" method="post" enctype="multipart/form-data">
                            @else
                            <form action="{{ route('admin.slider.update') }}" method="post">
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                @csrf
                                <input type="hidden" name="id" value="{{ @$data->id}}">
                                <div class="form-group">
                                    <label>Text</label>
                                    <input name="text" type="text" class="form-control" value="{{ @$data->text }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Slider</label>
                                    <input type="file" name="image" class="form-control-file"
                                        id="exampleFormControlFile1">
                                </div>
                                <div class="block_button" style="float: right;">
                                    <span id="res_save2" style="display: none">Saved!</span> <button id="save_contact"
                                        type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- footer -->
        @include('includeadmin.footer')
        <!-- end footer -->
    </div>
    <!-- ./wrapper -->
    @include('includeadmin.foot')
