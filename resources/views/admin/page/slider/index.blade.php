@include('includeadmin.header')

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        @include('includeadmin.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('includeadmin.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Post trip</h1>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="card">
                    <div class="card-header">
                        <div class="p-2 bd-highlight">
                            <h3 class="card-title">Blog List</h3>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a class="btn btn-success" href="{{ route('admin.slider.create')}}" role="button">List
                                Add New Slider</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Slider</th>
                                    <th>Text</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $i => $item)
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $item->image }}</td>
                                    <td>{{ $item->text }}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a data-toggle="tooltip" data-placement="right" title="Edit" href="slider/edit/{{$item->id}}"
                                                class="btn btn-warning">
                                                <i class="text-white far fa-edit"></i>
                                            </a>
                                            <a data-toggle="tooltip" data-placement="right" title="Delete" href="slider/delete/{{$item->id}}"
                                                class="btn btn-danger">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    </form>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- footer -->
        @include('includeadmin.footer')
        <!-- end footer -->
    </div>
    <!-- ./wrapper -->
    @include('includeadmin.foot')
