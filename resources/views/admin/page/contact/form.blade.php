
@include('includeadmin.header')
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Navbar -->
		@include('includeadmin.navbar')
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		@include('includeadmin.sidebar')

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0 text-dark">Post trip</h1>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">
							<i class="ion ion-clipboard mr-1"></i>
							Contact Information
						</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
					<form action="{{ route('admin.contact.update')}}" method="post">
						@csrf
					<input type="hidden" name="id" value="{{ $data->id }}">
								
							
							<div class="box-body">
								<!-- text input -->
								<div class="form-group">
									<label>Address/Street</label>
								<input type="text" id="address" name="address" class="form-control" placeholder="Enter ..." value="{{ $data->address }}">
								</div>
								<div class="form-group">
									<label>Phone</label>
									<input type="text" id="phone" name="phone" class="form-control" placeholder="Enter ..." value="">
								</div>
								<div class="form-group">
									<label>Email</label>
									<input type="text" id="email" name="email" class="form-control" placeholder="Enter ..." value="">
								</div>
								<div class="form-group">
									<label>Instagram User</label>
									<input type="text" id="instagram" name="instagram" class="form-control" placeholder="Enter ..." value="-">
								</div>
								<div class="form-group">
									<label>WhatsApp Number</label>
									<input type="text" id="whatsapp" name="whatsapp" class="form-control" placeholder="Enter ..." value="">
								</div>
								<div class="form-group">
									<label>Facebook URL</label>
									<input type="text" id="facebook" name="facebook" class="form-control" placeholder="Enter ..." value="">
								</div>
								<div class="form-group">
									<label>Google Maps</label>
									<input type="text" class="form-control target_embed" name="google" placeholder="Enter Embed Script ...">
								</div>
								<div class="block_button" style="float: right;">
									<span id="res_save2" style="display: none">Saved!</span> <button id="save_contact" type="submit" class="btn btn-success">Save</button>
								</div>
								
							</div>
						</div>
					</form>
				</div>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<!-- footer -->
		@include('includeadmin.footer')
		<!-- end footer -->
	</div>
	<!-- ./wrapper -->
	@include('includeadmin.foot')