
@include('includeadmin.header')
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Navbar -->
		@include('includeadmin.navbar')
		<!-- /.navbar -->

		<!-- Main Sidebar Container -->
		@include('includeadmin.sidebar')

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0 text-dark">Post trip</h1>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">
							<i class="ion ion-clipboard mr-1"></i>
							To Do List
						</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<form action="{{ route('admin.general.update') }}" method="post">
							@foreach($data as $item)
							<input type="hidden" name="id" value="{{$item->id}}">
							<div class="form-group">
								<label>Web Title</label>
								<input name="webtitle" type="text" class="form-control" value="{{ $item->webtitle }}">
							</div>
							<div class="form-group">
								<label for="exampleFormControlTextarea1">Meta</label>
								<textarea name="meta" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
							</div>
							<div class="form-group">
								<label for="exampleFormControlFile1">Logo</label>
								<input type="file" class="form-control-file" id="exampleFormControlFile1">
							</div>
							@endforeach
						</div>
					<div class="card-footer clearfix">
						<a href="admin/general/edit/{{ $item->id }}" class="btn btn-info float-right">Edit</a>
					</div>
				</form>
				</div>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<!-- footer -->
		@include('includeadmin.footer')
		<!-- end footer -->
	</div>
	<!-- ./wrapper -->
	@include('includeadmin.foot')