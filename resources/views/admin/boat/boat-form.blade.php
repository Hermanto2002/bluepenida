@include('includeadmin.header')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    @include('includeadmin.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('includeadmin.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Post boat</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Post boat form</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           @if($form_mode == 'add')
           <form action="{{ route('admin.boat.store') }}" method="post"
           enctype="multipart/form-data">
           @else
           <form action="{{ route('admin.boat.update') }}" method="post">
            <input type="hidden" name="id" value="{{ @$data->id }}">
            @endif
            @csrf
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group">
                  <label>Name</label>
                  <input name="title" type="text" class="form-control" value="{{ @$data->title }}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-3">
                <div class="form-group form-check">
                  <input @if(@$data->life_jacket=='on')
                  checked @endif name="life_jacket" type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">life jacket</label>
                </div>
              </div>
              <div class="col-12 col-md-9">
                <div class="form-group form-check">
                  <input @if(@$data->baggage=='on')
                  checked @endif name="baggage" type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">baggage</label>
                </div>
              </div>
              <div class="col-12 col-md-3">
                <div class="form-group form-check">
                  <input @if(@$data->insurance=='on')
                  checked @endif name="insurance" type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">insurance</label>
                </div>
              </div>
              <div class="col-12 col-md-9">
                <div class="form-group form-check">
                  <input @if(@$data->tax=='on')
                  checked @endif name="tax" type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">tax</label>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="form-group">
                  <label for="exampleFormControlFile1">Image</label>
                  <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <input type="hidden" name="type" value="Boat">
              </div>
            </div>
            <div style="text-align: right;">
              <button class="btn btn-success">Post</button>
            </div>

          </div>
        </div>
        
      </form>
    </div>
    <!-- /.card-body -->
  </div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- footer -->
@include('includeadmin.footer')
<!-- end footer -->
</div>
<!-- ./wrapper -->
<script type="text/javascript">
  $('#timepicker').datetimepicker({
    format: 'LT'
  })
</script>
@include('includeadmin.foot')