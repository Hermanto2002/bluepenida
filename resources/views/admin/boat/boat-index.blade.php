@include('includeadmin.header')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    @include('includeadmin.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('includeadmin.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Boat</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="card">
          <div class="card-header">
            <div class="d-flex justify-content-between">
              <div class="p-2 bd-highlight">
                <h3 class="card-title">Boat List</h3>
              </div>
              <div class="p-2 bd-highlight">
                <a class="btn btn-success" href="{{ route('admin.boat.create')}}" role="button">Create Boat</a>
              </div>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Atribut</th>
                  <th>Created at</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $i => $item)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td>{{$item->title}}</td>
                  <td>
                    <img width="200px" src="{{ asset('images/boat/'.$item->image)}}"></td>
                  <td>
                    @if($item->life_jacket=='on')
                    <i data-toggle="tooltip" data-placement="top" title="life jacket" class="material-icons">filter_tilt_shift
                    </i>
                    @endif
                    @if($item->baggage=='on')
                    <i data-toggle="tooltip" data-placement="top" title="baggage" class="material-icons">
                    business_center
                    </i>
                    @endif
                    @if($item->insurance=='on')
                    <i data-toggle="tooltip" data-placement="top" title="insurance" class="material-icons">favorite
                    </i>
                    @endif
                    @if($item->tax=='on')
                    <i data-toggle="tooltip" data-placement="top" title="tax" class="material-icons">local_atm
                    </i>
                    @endif
                  </td>
                  <td>{{$item->created_at}}</td>
                  <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <a data-toggle="tooltip" data-placement="right" title="Edit" href="/admin/boat/edit/{{$item->id}}" class="btn btn-warning">
                        <i class="text-white far fa-edit"></i>
                      </a>
                      <a data-toggle="tooltip" data-placement="right" title="Delete" href="/admin/boat/delete/{{$item->id}}" class="btn btn-danger">
                        <i class="far fa-trash-alt"></i>
                      </a>
                    </div>
                  </td>
                  
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- footer -->
    @include('includeadmin.footer')
    <!-- end footer -->
  </div>
  <!-- ./wrapper -->
  @include('includeadmin.foot')