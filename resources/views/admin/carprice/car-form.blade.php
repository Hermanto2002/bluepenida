@include('includeadmin.header')
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    @include('includeadmin.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('includeadmin.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Post boat</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Post boat form</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           @if($form_mode == 'add')
           <form action="{{ route('admin.carprice.store') }}" method="post"
           enctype="multipart/form-data">
           @else
           <form action="{{ route('admin.carprice.update') }}" method="post">
            <input type="hidden" name="id" value="{{ @$data->id }}">
            @endif
            @csrf
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group">
                  <label>Price</label>
                  <input name="price" type="text" class="form-control" value="{{ @$data->price }}">
                </div>
                <div class="form-group">
                  <label>Car</label>
                  <select name="car_id" class="form-control" id="exampleFormControlSelect1">
                    @foreach($car as $item)
                    <option @if(@$data->from_id == $item->id) selected @endif value="{{$item->id}}">{{$item->title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label>Trip</label>
                  <select name="trip_id" class="form-control" id="exampleFormControlSelect1">
                    @foreach($trip as $item)
                    <option @if(@$data->from_id == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label>Pick Up Location</label>
                  <input name="pickup_from" type="text" class="form-control" value="{{ @$data->pickup_from }}">
                </div>
              </div>
            </div>
            <div style="text-align: right;">
              <button class="btn btn-success">Post</button>
            </div>

          </div>
        </div>
        
      </form>
    </div>
    <!-- /.card-body -->
  </div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- footer -->
@include('includeadmin.footer')
<!-- end footer -->
</div>
<!-- ./wrapper -->
<script type="text/javascript">
  $('#timepicker').datetimepicker({
    format: 'LT'
  })
</script>
@include('includeadmin.foot')