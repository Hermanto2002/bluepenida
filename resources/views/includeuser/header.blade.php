


<body>
	<nav class="navbar navbar-default navbar-sticky bootsnav">
		<div class="container">

			<!-- Start Atribute Navigation -->
			<div class="attr-nav">
				<ul>
					<li class="dropdown">
						<a href="{{ route('cart-index')}}" class="dropdown-toggle" data-toggle="dropdown" >
							<i class="fa-2x fa fa-shopping-bag"></i>
						</a>
						<!-- <ul class="dropdown-menu cart-list">
							<li>
								<a href="#" class="photo"><img src="IMAGE_ADDRESS" class="cart-thumb" alt="" /></a>
								<h6><a href="#">Delica omtantur </a></h6>
								<p>2x - <span class="price">$99.99</span></p>
							</li>
							<li>
								<a href="#" class="photo"><img src="IMAGE_ADDRESS" class="cart-thumb" alt="" /></a>
								<h6><a href="#">Delica omtantur </a></h6>
								<p>2x - <span class="price">$99.99</span></p>
							</li>
							-- More List --
							<li class="total">
								<span class="pull-right"><strong>Total</strong>: $0.00</span>
								<a href="#" class="btn btn-default btn-cart">Cart</a>
							</li>
						</ul> -->
					</li>
				</ul>
			</div>
			<!-- End Atribute Navigation -->


			<!-- Start Header Navigation -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
					<i class="fa fa-bars"></i>
				</button>
				@foreach($page as $item)
				<a class="navbar-brand" href="{{ route('index')}}"><img style="width: 100%;" src="{{ asset('images/page/'.$item->logo)}}"
						class="logo" alt=""></a>
				@endforeach
			</div>
			<!-- End Header Navigation -->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbar-menu">
				<ul class="nav navbar-nav navbar-center">
					<li><a href="{{ route('index')}}">HOME</a></li>
					<li><a href="{{ route('boat')}}">FAST BOAT</a></li>
					<li><a href="{{ route('car')}}">CAR TOUR</a></li>
					<li><a href="{{ route('checkbooking')}}">CHECK BOOKING</a></li>
					<li><a href="{{ url('/blog')}}">TOURIST INFORMATION</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>