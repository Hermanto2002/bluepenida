<section  class="travel-box" style="z-index: 2;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-travel-boxes">
					<div id="desc-tabs" class="desc-tabs">

						<ul class="nav nav-tabs" role="tablist" id="myDIV">

							<li role="presentation" class="tombol @if($type=='car') active @endif">
								<a href="#tours" aria-controls="tours" role="tab" data-toggle="tab">
									<i class="fas fa-car"></i>
									Car
								</a>
							</li>

							<li role="presentation" class="tombol @if($type=='boat') active @endif">
								<a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">
									<i class="fas fa-ship"></i>
									Boat
								</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">

							<div role="tabpanel" class="tab-pane @if($type=='car') active @endif fade in" id="tours">
								<div class="tab-para">
									<form action="{{ route('searchcar') }}" method="get">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">

													<h2>Pick up location</h2>

													<div class="travel-select-icon">
														<select name="from" class="form-control ">
															<option value="request">By request</option>
														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">
													<h2>Pick up date</h2>
													<div class="travel-check-icon">
														<form action="#">
															<input type="text" name="departure" class="form-control" data-toggle="datepicker"
															placeholder="12 -01 - 2017 ">
														</form>
													</div><!-- /.travel-check-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">
													<h2>Tour type</h2>
													<div class="travel-select-icon">
														<select name="trip_id" class="form-control ">
															@foreach($trip as $item)
															<option value="{{$item->id}}">{{$item->name}}</option>
															@endforeach
														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">
													<h2>Car quantity</h2>
													<div class="travel-select-icon">
														<select class="form-control ">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											
											<div class="col-sm-5">
												<div class="about-btn pull-right">
													<button  class="about-view travel-btn">
														search	
													</button><!--/.travel-btn-->
												</div><!--/.about-btn-->
											</div><!--/.col-->

										</div><!--/.row-->
									</form>
								</div><!--/.tab-para-->

							</div><!--/.tabpannel-->

							<div role="tabpanel" class="tab-pane @if($type=='boat') active @endif fade in" id="flights">
								<div class="tab-para">
									<form action="{{ route('searchboat') }}" method="get">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">

													<h2>from</h2>

													<div class="travel-select-icon">
														<select name="from" class="form-control ">

															@foreach($localtions as $item)
															@if($item->parent_id == 0)
															<option value="{{$item->id}}">{{$item->name}}</option>
															@endif
															@foreach($localtions as $sub)
															@if($sub->parent_id == $item->id)
															<option value="{{$sub->id}}">-- {{$sub->name}}</option>
															@endif
															@endforeach
															@endforeach

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">

													<h2>to</h2>

													<div class="travel-select-icon">
														<select name="to" class="form-control ">

															@foreach($localtions as $item)
															@if($item->parent_id == 0)
															<option value="{{$item->id}}">{{$item->name}}</option>
															@endif
															@foreach($localtions as $sub)
															@if($sub->parent_id == $item->id)
															<option value="{{$sub->id}}">-- {{$sub->name}}</option>
															@endif
															@endforeach
															@endforeach

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->

												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">
													<h2>Passanger</h2>
													<div class="travel-select-icon">
														<select name="passanger" class="form-control ">

															<option value="1">1</option><!-- /.option-->

															<option value="2">2</option><!-- /.option-->

															<option value="3">3</option><!-- /.option-->
															<option value="4">4</option><!-- /.option-->
															<option value="5">5</option><!-- /.option-->

														</select><!-- /.select-->
													</div><!-- /.travel-select-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->

											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="single-tab-select-box">
													<h2>departure</h2>
													<div class="travel-check-icon">
														<input type="text" name="departure" class="form-control" data-toggle="datepicker">
													</div><!-- /.travel-check-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<div class="col-lg-4 col-md-4 col-sm-12" id="LicenseCustomer" style="display:none">
												<div class="single-tab-select-box">
													<h2>return</h2>
													<div class="travel-check-icon">
														<form action="#">
															<input type="text" name="return" class="form-control" data-toggle="datepicker">
														</form>
													</div><!-- /.travel-check-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<div class="col-lg-5 col-md-5 col-sm-12">
												<div class="single-tab-select-box">
													<h2></h2>
													<div class="travel">
														<div class="single-trip-circle">
															<input type="checkbox" id="_My.notFinal"  name="radio" />
															<label for="radio01" style="margin-left: 10px: ">
																<span class="round-boarder">
																	<span class="round-boarder1"></span>
																</span>&nbsp;&nbsp;Return
															</label>
														</div><!--/.single-trip-circle-->
													</div><!-- /.travel-check-icon -->
												</div><!--/.single-tab-select-box-->
											</div><!--/.col-->
											<input type="hidden" name="type" value="Boat">
											<div class="clo-sm-5">
												<div class="about-btn pull-right">
													<button  class="about-view travel-btn">
														search	
													</button><!--/.travel-btn-->
												</div><!--/.about-btn-->
											</div><!--/.col-->

										</div><!--/.row-->
									</form>
								</div>

							</div><!--/.tabpannel-->

						</div><!--/.tab content-->
					</div><!--/.desc-tabs-->
				</div><!--/.single-travel-box-->
			</div><!--/.col-->
		</div><!--/.row-->
	</div><!--/.container-->

		</section><!--/.travel-box-->