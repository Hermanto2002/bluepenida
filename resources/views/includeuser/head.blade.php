<!doctype html>
<html class="no-js" lang="en">

<head>
	<!-- META DATA -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
	<!--font-family-->
	<link href="https://fonts.googleapis.com/css?family=Rufina:400,700" rel="stylesheet" />

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet" />

	<title>Blue Penida</title>

	<link rel="shortcut icon" type="image/icon" href="{{ asset('assets/logo/favicon.png')}}" />

	<!--font-awesome.min.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css')}}" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">

	<!--animate.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/animate.css')}}" />

	<!--hover.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/hover-min.css')}}">

	<!--datepicker.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/datepicker.css')}}">

	<!--owl.carousel.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css')}}" />

	<!-- range css-->
	<link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css')}}" />

	<!--bootstrap.min.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}" />
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"> -->

	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-grid.css')}}" />
	<link rel="stylesheet" href="{{ asset('assets/css/margin.css')}}" />

	<!-- bootsnav -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootsnav.css')}}" />

	<!--style.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/style.css')}}" />

	<!--responsive.css-->
	<link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v4.0&appId=504760163614029&autoLogAppEvents=1"></script>
</head>