<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light">Bluepenida</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
            data-accordion="false">
            <li class="nav-item ">
                <a href="{{ route('admin.blog')}}" class="nav-link active">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.booking')}}" class="nav-link">
                    <i class="fas fa-book"></i>
                    <p>Booking</p>
                </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="fas fa-book"></i>
                    <p>
                        Blog
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.blog')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Blog List</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.blog.category')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Boat Category</p>
                        </a>
                    </li>
                        
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="fas fa-car"></i>
                    <p>
                        Cars Management
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.car')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Cars</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.carprice')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Cars Prices</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.trip')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Trip</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="fas fa-ship"></i>
                    <p>
                        Boats Management
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.boat')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Boats</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.boatprice')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Boat Prices</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.localtions')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Locations</p>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-header">Page setting</li>
    </ul>
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
    data-accordion="false">
    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="fa fa-bars"></i>
            <p>
                Menu
                <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Header Menu</p>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="far fa-building"></i>
            <p>
                Property
                <i class="fas fa-angle-left right"></i>
            </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{ route('admin.boat')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Boats</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.boatprice')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Boat Prices</p>
                </a>
            </li>
        </ul>
    </li>
</ul>
</nav>
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>