<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carprice extends Model
{
    protected $table = 'tb_car_prices';
    protected $guarded = [];

    public function getTrip()
    {
        return $this->belongsTo('App\Trip','trip_id');
    }
    public function getCar()
    {
        return $this->belongsTo('App\Car','car_id');
    }

}
