<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'tb_blog';
    protected $guarded = [];
    public function getCategory()
    {
        return $this->belongsTo('App\Category','category_id');
    }
}
