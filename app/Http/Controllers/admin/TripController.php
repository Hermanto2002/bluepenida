<?php

namespace App\Http\Controllers\admin;


use Illuminate\Http\Request;
use App\Locations;
use App\Blog;
use App\Category;
use App\Trip;
use App\Post;
use App\Http\Controllers\Controller;
use File;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Trip::get();
        return view('admin.trip.trip-index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form_mode = 'add';
        return view('admin.trip.trip-form', compact('form_mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'name' => request('name'),
        ];

        Trip::create($data);

        return redirect()->route('admin.trip');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function show(Trip $trip)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function edit(Trip $trip, $id)
    {
        $data = Trip::find($id);
        $form_mode = 'edit';
        return view('admin.trip.trip-form', compact('data','form_mode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trip $trip)
    {
        $update = Trip::find($request->id);
        $data=[
            'name' => request('name'),
        ];
        
        $update->update($data);
        return redirect()->route('admin.trip');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trip  $trip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trip $trip, $id)
    {
        $trip = Trip::findOrFail($id);
        $trip->delete();
        return redirect()->route('admin.trip');
    }
}
