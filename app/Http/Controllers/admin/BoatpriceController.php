<?php

namespace App\Http\Controllers\admin;

use App\Boatprice;
use App\Boat;
use App\Locations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BoatpriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Boatprice::get();
        return view('admin.boatprice.boat-index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post=Boatprice::all();
        $form_mode = 'add';
        $localtions = Locations::all();
        $boat = Boat::all();
        return view('admin.boatprice.boat-form', compact('form_mode','post','localtions','boat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'price' => request('price'),
            'from_id' => request('from_id'),
            'to_id' => request('to_id'),
            'boat_id' => request('boat_id'),
            'time_departing' => request('time_departing'),
            'time_arrived' => request('time_arrived'),
        ];

        Boatprice::create($data);

        return redirect()->route('admin.boatprice');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Boatprice  $boatprice
     * @return \Illuminate\Http\Response
     */
    public function show(Boatprice $boatprice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Boatprice  $boatprice
     * @return \Illuminate\Http\Response
     */
    public function edit(Boatprice $boatprice, $id)
    {
        
        $data = Boatprice::find($id);
        $form_mode = 'edit';
        $localtions = Locations::all();
        $boat = Boat::all();
        return view('admin.boatprice.boat-form', compact('form_mode','data','localtions','boat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Boatprice  $boatprice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Boatprice $boatprice)
    {
        $update = Boatprice::find($request->id);
        $data=[
            'price' => request('price'),
            'from_id' => request('from_id'),
            'to_id' => request('to_id'),
            'boat_id' => request('boat_id'),
            'time_departing' => request('time_departing'),
            'time_arrived' => request('time_arrived'),
        ];
        
        $update->update($data);
        return redirect()->route('admin.boatprice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Boatprice  $boatprice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Boatprice $boatprice, $id)
    {
        $post = Boatprice::findOrFail($id);
        $post->delete();
        return redirect()->route('admin.boatprice');
    }
}
