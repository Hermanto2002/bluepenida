<?php

namespace App\Http\Controllers\admin;

use App\Carprice;
use App\Car;
use App\Trip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarpriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Carprice::get();
        return view('admin.carprice.car-index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post=Carprice::all();
        $form_mode = 'add';
        $trip = Trip::all();
        $car = Car::all();
        return view('admin.carprice.car-form', compact('form_mode','post','trip','car'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'trip_id' => request('trip_id'),
            'price' => request('price'),
            'car_id' => request('car_id'),
            'pickup_from' => request('pickup_from'),
        ];

        Carprice::create($data);

        return redirect()->route('admin.carprice');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Carprice  $Carprice
     * @return \Illuminate\Http\Response
     */
    public function show(Carprice $carprice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Carprice  $Carprice
     * @return \Illuminate\Http\Response
     */
    public function edit(Carprice $carprice, $id)
    {
        
        $data = Carprice::find($id);
        $form_mode = 'edit';
        $trip = Trip::all();
        $car = Car::all();
        return view('admin.carprice.car-form', compact('form_mode','data','trip','car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Carprice  $Carprice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carprice $carprice)
    {
        $update = Carprice::find($request->id);
        $data=[
            'trip_id' => request('trip_id'),
            'price' => request('price'),
            'car_id' => request('car_id'),
            'pickup_from' => request('pickup_from'),
        ];
        
        $update->update($data);
        return redirect()->route('admin.carprice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Carprice  $Carprice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carprice $carprice, $id)
    {
        $post = Carprice::findOrFail($id);
        $post->delete();
        return redirect()->route('admin.carprice');
    }
}
