<?php

namespace App\Http\Controllers\admin;


use Illuminate\Http\Request;
use App\Locations;
use App\Blog;
use App\Category;
use App\Trip;
use App\Boat;
use App\Http\Controllers\Controller;
use File;


class PostboatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	// $type='Boat';
     //    $data = Boat::where('type', $type);
        $data = Boat::all();
        return view('admin.boat.boat-index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post=Boat::all();
        $form_mode = 'add';
        $localtions = Locations::all();
        return view('admin.boat.boat-form', compact('form_mode','post','localtions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'life_jacket' => request('life_jacket'),
            'baggage' => request('baggage'),
            'insurance' => request('insurance'),
            'tax' => request('tax')
        ];
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/boat/', $filename);
            $data['image'] = $filename;
        }

        Boat::create($data);

        return redirect()->route('admin.boat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Boat $boat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Boat $boat, $id)
    {
        $post = Boat::get();
        $data = Boat::find($id);
        $form_mode = 'edit';
        $localtions = Locations::all();
        return view('admin.boat.boat-form', compact('form_mode','post','data','localtions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Boat $boat)
    {
        $update = Boat::find($request->id);
         $data=[
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'life_jacket' => request('life_jacket'),
            'baggage' => request('baggage'),
            'insurance' => request('insurance'),
            'tax' => request('tax')
        ];
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/boat/', $filename);
            $data['image'] = $filename;
        }
        $update->update($data);
        return redirect()->route('admin.boat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Boat $boat, $id)
    {
        $post = Boat::findOrFail($id);
        $post->delete();
        return redirect()->route('admin.boat');
    }
}
