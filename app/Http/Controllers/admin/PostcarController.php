<?php

namespace App\Http\Controllers\admin;


use Illuminate\Http\Request;
use App\Locations;
use App\Blog;
use App\Category;
use App\Trip;
use App\Car;
use App\Http\Controllers\Controller;
use File;

class PostcarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	//$type='Boat';
     	//data = Post::where('type', $type);
        $data = Car::get();
        return view('admin.car.car-index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form_mode = 'add';
        $trip = Trip::all();
        return view('admin.car.car-form', compact('form_mode','trip'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'include_driver' => request('include_driver'),
            'include_fuel' => request('include_fuel'),
            'max_person' => request('max_person'),
            'max_bag' => request('max_bag'),
        ];
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/car/', $filename);
            $data['image'] = $filename;
        }

        Car::create($data);

        return redirect()->route('admin.car');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car, $id)
    {
        $data = Car::find($id);
        $form_mode = 'edit';
        $trip = Trip::all();
        return view('admin.car.car-form', compact('form_mode','data','trip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $update = Car::find($request->id);
        $data=[
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'include_driver' => request('include_driver'),
            'include_fuel' => request('include_fuel'),
            'max_person' => request('max_person'),
            'max_bag' => request('max_bag'),
        ];
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time().'.'.$extension;
            $file->move('images/car/', $filename);
            $data['image'] = $filename;
        }
        $update->update($data);
        return redirect()->route('admin.car');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car, $id)
    {
        $post = Car::findOrFail($id);
        $post->delete();
        return redirect()->route('admin.car');
    }
}
