<?php

namespace App\Http\Controllers\admin;


use Illuminate\Http\Request;
use App\Locations;
use App\Blog;
use App\Category;
use App\Trip;
use App\Post;
use App\Http\Controllers\Controller;
use File;

class LocaltionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Locations::get();
        return view('admin.locations.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $localtions=Locations::all();
        $form_mode = 'add';
        return view('admin.locations.form', compact('form_mode','localtions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'name' => request('name'),
            'parent_id' => request('parent_id'),

        ];

        Locations::create($data);

        return redirect()->route('admin.localtions');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Localtions  $localtions
     * @return \Illuminate\Http\Response
     */
    public function show(Locations $locations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Localtions  $localtions
     * @return \Illuminate\Http\Response
     */
    public function edit(Locations $locations, $id)
    {
        $localtions = Locations::get();
        $data = Locations::find($id);
        $form_mode = 'edit';
        return view('admin.locations.form', compact('form_mode','localtions','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Localtions  $localtions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Locations $locations)
    {
        $update = Locations::find($request->id);
        $data=[
            'name' => request('name'),
            'parent_id' => request('parent_id'),
        ];
        
        $update->update($data);
        return redirect()->route('admin.localtions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Localtions  $localtions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Locations $locations, $id)
    {
        $localtions = Locations::findOrFail($id);
        $localtions->delete();
        return redirect()->route('admin.localtions');
    }
}
