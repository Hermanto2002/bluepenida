<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Boatprice;
use App\Carprice;
use App\Locations;
use App\Booking;
use App\Page;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailBooking;

use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
	public function index(Request $request)
	{        
        // $boat = Session::get('cartboat'); 
        // $car = Session::get('cartcar');
        // $boat = Session::has('cartboat');
        // $car = Session::has('cartcar');
  		//$boat = $request->session()->has('cartboat');
		// $car = $request->session()->has('cartcar');
		$boat = $request->session()->get('cartboat');
		$car = $request->session()->get('cartcar');
		$page=Page::all();
		$priceboat= $boat['price'] * $boat['passanger'];
		$pricecar= $car['price'];
		$total = $priceboat + $pricecar;
		// dd($total);
		return view('user.cart', compact('page','boat','car','priceboat','total'));
	}

	public function addToCartBoat(Request $request, $id)
	{
		// $tbboat = DB::select('select * from tb_boat_prices where id='.$id);
		$departure = $request->get('departure');
		$passanger = $request->get('passanger');
		$tbboat = Boatprice::where('id',$id)->get();
        // $product = Boatprice::where('id', $id);
		$boat = Session::get('cartboat');
		// dd($boat[8]['id']);
		$boat = array(
			"id" => $tbboat[0]->id,
			"name" => $tbboat[0]->getBoat->title,
			"price" => $tbboat[0]->price,
			"from_id" => $tbboat[0]->getFrom->name,
			"to_id" => $tbboat[0]->getTo->name,
			"boat_id" => $tbboat[0]->getBoat->boat_id,
			"time_departing" => $tbboat[0]->time_departing,
			"time_arrived" => $tbboat[0]->time_arrived,
			"created_at" => $tbboat[0]->created_at,
			"updated_at" => $tbboat[0]->updated_at,
			"departure" => $departure,
			"passanger" => $passanger,
		); 
		Session::put('cartboat', $boat);

			// dd($boat);
		
		return redirect()->route('cart-index');
	}

	public function addToCartCar(Request $request, $id)
	{
		$tbcar = Carprice::where('id',$id)->get();
		$departure = $request->get('departure');
		$pickdetail = $request->get('pickdetail');
		$picktime = $request->get('picktime');
		$pickrequest = $request->get('pickrequest');
		// dd($departure);
        // $product = carprice::where('id', $id);
		$car = Session::get('cartcar');
		$car = array(
			"id" => $tbcar[0]->id,
			"trip_id" => $tbcar[0]->getTrip->trip_id,
            "name" => $tbcar[0]->getCar->title,
            "price" => $tbcar[0]->price,
			"pickup_from" => $tbcar[0]->pickup_from,
			"departure"=> $departure,
			"pickdetail"=> $pickdetail,
			"picktime"=> $picktime,
			"pickrequest"=> $pickrequest,
		);  
		Session::put('cartcar', $car); 

		return redirect()->route('cart-index');

	}

	public function saveCart(Request $request)
	{
		// $firsname = $request->get('firsname');
		// $phone = $request->get('phone');
		// $email = $request->get('email');
		// $carid = $request->get('carid');
		$pricecar = $request->get('pricecar');
		$priceboat = $request->get('priceboat');
		$price = $pricecar+$priceboat;
		$number = mt_rand(10000, 99999);
		$cek = Booking::where('booking_code',$number)->exists();

		if ($cek == true) {
			return redirect()->route('cart-save');
		}
		$data=[
            'carid' => request('carid'),
			'boatid' => request('boatid'),
			'nama' => request('firsname'),
			'email' => request('email'),
			'phone' => request('phone'),
			'price' => $price,
			'booking_code' => $number,
		]; 
        // dd($data['email']);
        Mail::to($data['email'])->send(new EmailBooking($data));
		Booking::create($data);
		
       	return redirect()->route('checkbooking')->with('success', 'Thanks for contacting us!');
	}

	public function deleteCartBoat($id)
	{
		$cart = Session::get('cartboat');
		Session::forget('cartboat');
		return redirect()->back();
	}


	public function deleteCartCar($id)
	{
		$cart = Session::get('cartcar');
		Session::forget('cartcar');
		return redirect()->back();
	}

	public function checkbooking()
	{
		$page=Page::all();
		return view('user.checkbooking', compact('page'));
	}

	public function checkbookingresult(Request $request)
	{
		$page=Page::all();
		$code=$request->get('code_booking');
		$email=$request->get('email');
		$result = Booking::where(function($query) use ($code, $email) {
			$query->where('booking_code', 'LIKE', '%'.$code.'%')
			->where('email', 'LIKE', '%'.$email.'%');
		})->paginate(10);
		return view('user.bookingcheckresult', compact('page','result'));
	}
}
