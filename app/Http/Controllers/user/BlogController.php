<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Blog;
use App\Category;
use App\Page;

use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index()
    {
        $page=Page::all();
    	$post= Blog::paginate(10);
    	$categorymenu = Category::all();
    	$newpost=Blog::Orderby('id', 'desc')->limit(5)->get();
    	return view('user.blog', compact('page','post','categorymenu','newpost'));
    }
    public function show($slug)
    {
        $page=Page::all();
    	$blog=Blog::where('slug',$slug)->first();
    	$categorymenu = Category::all();
    	$newpost=Blog::Orderby('id', 'desc')->limit(5)->get();
        return view('user.blogshow', compact('page','blog','categorymenu','newpost'));
    }
    public function category($category_id)
    {
    	$post = blog::where('category_id', $category_id)->paginate(6);
    	$categorymenu = Category::all();
    	$newpost=Blog::Orderby('id', 'desc')->limit(5)->get();
        return view('user.blog', compact('post','categorymenu','newpost'));
    }
    public function search(Request $request)
    {
    	$search = $request->get('title');
    	$categorymenu = Category::all();
    	$newpost=Blog::Orderby('id', 'desc')->limit(5)->get();
    	$post= Blog::all();
    	return view('user.blog', compact('post','categorymenu','newpost'));
    }
}
