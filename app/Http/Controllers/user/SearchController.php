<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Locations;
use App\Blog;
use App\Category;
use App\Trip;
use App\Boat;
use App\Boatprice;
use App\Carprice;
use App\Page;

use File;

class SearchController extends Controller
{
	public function searchboat(Request $request)
	{
		$page=Page::all();
		$localtions = Locations::all();
		$trip = Trip::all();
		$passanger = $request->get('passanger');
		$departure = $request->get('departure');
		$from = $request->get('from');
		$to = $request->get('to');
 		$result = Boatprice::where(function($query) use ($from, $to) {
			$query->where('from_id', 'LIKE', '%'.$from.'%')
			->where('to_id', 'LIKE', '%'.$to.'%');
		})->paginate(10);
		$type='boat';
		$search='true';
		return view('user.boatresult', compact('search','page','type','result','localtions','trip','departure','passanger','from','to'));
	}

	public function searchcar(Request $request)
	{
		$page=Page::all();
		$trip = Trip::all();
		$localtions = Locations::all();
		$trip_id = $request->get('trip_id');
		$passanger = $request->get('passanger');
		$departure = $request->get('departure');
		// $return = $request->get('return');

		$result = Carprice::where(function($query) use ($trip_id) {
			$query->where('trip_id', 'LIKE', '%'.$trip_id.'%');
				  // ->where('time_departing', 'LIKE', '%'.$departure.'%');
				  // ->where('time_arrived', 'LIKE', '%'.$return.'%');
		})->paginate(10);
		$type='car';
		$search='true';
		// dd($departure);
		return view('user.carresult', compact('search','page','type','result','localtions','trip','departure','passanger'));
	}

	public function boat(Request $request)
	{
		$page=Page::all();
		$localtions = Locations::all();
		$trip = Trip::all();
		$passanger = $request->get('passanger');
		$departure = $request->get('departure');
		$from = $request->get('from');
		$to = $request->get('to');
		$type = $request->get('type');

		$result = Boatprice::where(function($query) use ($from, $to) {
			$query->where('from_id', 'LIKE', '%'.$from.'%')
			->where('to_id', 'LIKE', '%'.$to.'%');
		})->paginate(10);
		$type='boat';
		$search='false';
		return view('user.boatresult', compact('search','page','type','result','localtions','trip','departure','passanger','from','to'));
	}

	public function car(Request $request)
	{
		$page=Page::all();
		$trip = Trip::all();
		$localtions = Locations::all();
		$trip_id = $request->get('trip_id');
		$passanger = $request->get('passanger');
		$departure = $request->get('departure');
		// $return = $request->get('return');

		$result = Carprice::where(function($query) use ($trip_id) {
			$query->where('trip_id', 'LIKE', '%'.$trip_id.'%');
				  // ->where('time_departing', 'LIKE', '%'.$departure.'%');
				  // ->where('time_arrived', 'LIKE', '%'.$return.'%');
		})->paginate(10);
		$type='car';
		$search='false';
		return view('user.carresult', compact('search','page','type','result','localtions','trip','departure','passanger'));
	}
}
