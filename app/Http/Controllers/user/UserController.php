<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Locations;
use App\Blog;
use App\Category;
use App\Trip;
use App\Post;
use App\Page;
use App\Slider;
use App\Http\Controllers\Controller;
use File;

class UserController extends Controller
{
    public function index()
    {
        $slider = Slider::all();
        $type='car';
        $page=Page::all();
    	$localtions = Locations::all();
        $trip=Trip::Orderby('id', 'desc')->limit(6)->get();
    	$blog=Blog::Orderby('id', 'desc')->limit(6)->get();
    	return view('user.index', compact('type','localtions','trip','blog','page','slider'));
    }
    public function cart()
    {
    	return view('user.cart');
    }
    public function checkout()
    {
    	return view('user.checkout');
    }
}
