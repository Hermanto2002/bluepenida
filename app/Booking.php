<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'tb_booking';
    protected $guarded = [];

    public function getBoat()
    {
        return $this->belongsTo('App\Boat','boat_id');
    }
    public function getCar()
    {
        return $this->belongsTo('App\Car','car_id');
    }
}
