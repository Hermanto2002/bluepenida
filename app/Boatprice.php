<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boatprice extends Model
{
    protected $table = 'tb_boat_prices';
    protected $guarded = [];

    public function getFrom()
    {
        return $this->belongsTo('App\Locations','from_id');
    }
    public function getTo()
    {
        return $this->belongsTo('App\Locations','to_id');
    }
    public function getBoat()
    {
        return $this->belongsTo('App\Boat','boat_id');
    }

}
