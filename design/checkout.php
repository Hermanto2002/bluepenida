<?php include'header.php' ?>

<nav aria-label="breadcrumb bg-white border">
    <ol class="breadcrumb bg-white container" style="background-color:white;">
        <li class="breadcrumb-item mx-4">
            <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>
<div class="container">
    <div class="row mt-3 mb-5">
        <div class="col-sm-8">
            <h4>Billing details</h4>
            <div class="row">
                <form>
                    <div class="form-row mt-4">
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">First name *</label>
                            <input type="text" class="form-control" placeholder="First name">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Last name *</label>
                            <input type="text" class="form-control" placeholder="Last name">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Company name (optional)</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Country *</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Street address *</label>
                            <input type="text" class="form-control" placeholder="House number and street name">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Apartment, suite, unit etc. (optional) (optional)</label>
                            <input type="text" class="form-control"
                                placeholder="Apartment, suite, unit etc. (optional) (optional)">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Town / City *</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Province *</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="col-sm-6 mb-4">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Postcode / ZIP *</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Email address *</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-sm-6 mb-4">
                            <label for="formGroupExampleInput">Phone *</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                    </div>
                </form>
                <p>Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our <a href="">privacy policy</a>. </p>
                <button type="submit" class="btn btn-primary mb-2 mt-2">Proceed to PayPal</button>
            </div>
        </div>
        <div class="col-sm-4">
            <h4>Your Booking</h4>
            <div class="card w-100 mt-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <h4 href="">Half Day Tour West Nusa Penida</h4>
                            <p class="address"><i class="input-icon field-icon fa"><svg width="15px" height="15px"
                                        viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->

                                        <desc>Created with Sketch.</desc>
                                        <defs></defs>
                                        <g id="Ico_maps" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                            stroke-linecap="round" stroke-linejoin="round">
                                            <g id="Group" transform="translate(4.000000, 0.000000)" stroke="#666666">
                                                <g id="pin-1" transform="translate(-0.000000, 0.000000)">
                                                    <path
                                                        d="M15.75,8.25 C15.75,12.471 12.817,14.899 10.619,17.25 C9.303,18.658 8.25,23.25 8.25,23.25 C8.25,23.25 7.2,18.661 5.887,17.257 C3.687,14.907 0.75,12.475 0.75,8.25 C0.75,4.10786438 4.10786438,0.75 8.25,0.75 C12.3921356,0.75 15.75,4.10786438 15.75,8.25 Z"
                                                        id="Shape"></path>
                                                    <circle id="Oval" cx="8.25" cy="8.25" r="3"></circle>
                                                </g>
                                            </g>
                                        </g>
                                    </svg></i>Angel Billabong Nusa Penida
                            </p>
                            <p>$148,41</p>
                            <h4 class="mt-3">Subtotal  : $148,41</h4>
                            <h4 class="mt-3">Total  : $148,41</h4>
                        </div>
                        <div class="col-sm-4">
                            <img src="https://bluepenida.com/wp-content/uploads/2019/06/DJI_0038-150x150.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include'footer.php' ?>