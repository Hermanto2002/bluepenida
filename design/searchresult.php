<?php include'header.php' ?>


<section id="home" class="about-us-search">
	<div class="container">
		<div class="about-us-content">
			<div class="row">
				
			</div><!--/.row-->
		</div><!--/.about-us-content-->
	</div><!--/.container-->

</section><!--/.about-us-->
<!--about-us end -->

<!--travel-box start-->
<section  class="travel-box">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-travel-boxes">
					<div id="desc-tabs" class="desc-tabs">

						<ul class="nav nav-tabs" role="tablist" id="myDIV">

							<li role="presentation" class="tombol active">
								<a href="#tours" aria-controls="tours" role="tab" data-toggle="tab">
									<i class="fa fa-tree"></i>
									Tours
								</a>
							</li>

							<li role="presentation" class="tombol">
								<a href="#hotels" aria-controls="hotels" role="tab" data-toggle="tab">
									<i class="fa fa-building"></i>
									Room
								</a>
							</li>

							<li role="presentation" class="tombol">
								<a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">
									<i class="fas fa-ship"></i>
									Boat
								</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">

							<div role="tabpanel" class="tab-pane active fade in" id="tours">
								<div class="tab-para">

									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">

												<h2>destination</h2>

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your destination country</option><!-- /.option-->

														<option value="turkey">turkey</option><!-- /.option-->

														<option value="russia">russia</option><!-- /.option-->
														<option value="egept">egypt</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->

											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>From</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_in" class="form-control" data-toggle="datepicker" placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>To</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_out" class="form-control"  data-toggle="datepicker" placeholder="22 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->



										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>Person</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="4">4</option><!-- /.option-->
														<option value="8">8</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

									</div><!--/.row-->

									<div class="row">

										<div class="clo-sm-7">
											<div class="about-btn travel-mrt-0 pull-right">
												<button  class="about-view travel-btn">
													search	
												</button><!--/.travel-btn-->
											</div><!--/.about-btn-->
										</div><!--/.col-->

									</div><!--/.row-->

								</div><!--/.tab-para-->

							</div><!--/.tabpannel-->

							<div role="tabpanel" class="tab-pane fade in" id="hotels">
								<div class="tab-para">

									<div class="row">
										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>check in</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_in" class="form-control" data-toggle="datepicker" placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>check out</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_out" class="form-control"  data-toggle="datepicker" placeholder="22 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>Adult</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="3">3</option><!-- /.option-->
														<option value="4">4</option><!-- /.option-->
														<option value="5">5</option><!-- /.option-->
													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>Child</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="3">3</option><!-- /.option-->
														<option value="4">4</option><!-- /.option-->
														<option value="5">5</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

									</div><!--/.row-->

									<div class="row">
										<div class="col-sm-5"></div><!--/.col-->
										<div class="clo-sm-7">
											<div class="about-btn travel-mrt-0 pull-right">
												<button  class="about-view travel-btn">
													search	
												</button><!--/.travel-btn-->
											</div><!--/.about-btn-->
										</div><!--/.col-->

									</div><!--/.row-->

								</div><!--/.tab-para-->

							</div><!--/.tabpannel-->

							<div role="tabpanel" class="tab-pane fade in" id="flights">
								<div class="tab-para">
									<div class="trip-circle">
										<div class="single-trip-circle">
											<input type="radio" id="radio01" name="radio" />
											<label for="radio01">
												<span class="round-boarder">
													<span class="round-boarder1"></span>
												</span>round trip
											</label>
										</div><!--/.single-trip-circle-->
										<div class="single-trip-circle">
											<input type="radio" id="radio02" name="radio" />
											<label for="radio02">
												<span class="round-boarder">
													<span class="round-boarder1"></span>
												</span>on way
											</label>
										</div><!--/.single-trip-circle-->
									</div><!--/.trip-circle-->
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">

												<h2>from</h2>

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your location</option><!-- /.option-->

														<option value="turkey">turkey</option><!-- /.option-->

														<option value="russia">russia</option><!-- /.option-->
														<option value="egept">egypt</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>departure</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="departure" class="form-control" data-toggle="datepicker"
														placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>return</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="return" class="form-control" data-toggle="datepicker" placeholder="22 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>adults</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">5</option><!-- /.option-->

														<option value="10">10</option><!-- /.option-->

														<option value="15">15</option><!-- /.option-->
														<option value="20">20</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>childs</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="4">4</option><!-- /.option-->
														<option value="8">8</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

									</div><!--/.row-->

									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">

												<h2>to</h2>

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your destination location</option><!-- /.option-->

														<option value="istambul">istambul</option><!-- /.option-->

														<option value="mosko">mosko</option><!-- /.option-->
														<option value="cairo">cairo</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->

											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->
										<div class="col-lg-3 col-md-3 col-sm-4">
											<div class="single-tab-select-box">

												<h2>class</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter class</option><!-- /.option-->

														<option value="A">A</option><!-- /.option-->

														<option value="B">B</option><!-- /.option-->
														<option value="C">C</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->
										<div class="col-lg-3 col-md-3 col-sm-4">
											<div class="single-tab-select-box">

												<h2>Flexibility</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter class</option><!-- /.option-->

														<option value="A">A</option><!-- /.option-->

														<option value="B">B</option><!-- /.option-->
														<option value="C">C</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->
										<div class="clo-sm-5">
											<div class="about-btn pull-right">
												<button  class="about-view travel-btn">
													search	
												</button><!--/.travel-btn-->
											</div><!--/.about-btn-->
										</div><!--/.col-->

									</div><!--/.row-->

								</div>

							</div><!--/.tabpannel-->

						</div><!--/.tab content-->
					</div><!--/.desc-tabs-->
				</div><!--/.single-travel-box-->
				<div class="row" style="border-bottom: solid 1px gray; ">
					<div class="col-12" style="padding: 10px 15px">
						<h2 class="text-light">Diamond Beach: 5 tours found</h2>
					</div>
				</div>

				<div class="row py-3 px-2 mt-2">
					<div class="col-md-1">
						<h3>Filter</h3>
					</div>
					<div class="col-md-11">
						<div class="btn-group mr-3 mb-2">
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Review Score
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<a class="dropdown-item" href="#">Something else here</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Separated link</a>
							</div>
						</div>
						<div class="btn-group mr-3 mb-2">
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Tout Type
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<a class="dropdown-item" href="#">Something else here</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Separated link</a>
							</div>
						</div>
						<div class="btn-group mr-3 mb-2">
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Duration
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<a class="dropdown-item" href="#">Something else here</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Separated link</a>
							</div>
						</div>
						<div class="btn-group mr-3 mb-2">
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Language
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<a class="dropdown-item" href="#">Something else here</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Separated link</a>
							</div>
						</div>
						<div class="btn-group mr-3 mb-2">
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Sort
							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<a class="dropdown-item" href="#">Something else here</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#">Separated link</a>
							</div>
						</div>
					</div>
				</div>
			</div><!--/.col-->
		</div><!--/.row-->
	</div><!--/.container-->
</section><!--/.travel-box-->
<section>
	<div class="container">
		<div class="mx-3">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="single-package-item">
						<img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg" alt="package-place">
						<div class="single-package-item-txt">
							<h3>Nusa Penida<span class="pull-right">$499</span></h3>
							<div class="packages-para">
								<p>
									<span>
										<i class="fa fa-angle-right"></i> 5 daays 6 nights
									</span>
									<i class="fa fa-angle-right"></i>  5 star accomodation
								</p>
								<p>
									<span>
										<i class="fa fa-angle-right"></i>  transportation
									</span>
									<i class="fa fa-angle-right"></i>  food facilities
								</p>
							</div><!--/.packages-para-->
							<div class="packages-review">
								<p>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<span>2544 review</span>
								</p>
							</div><!--/.packages-review-->
							<div class="about-btn">
								<button  class="about-view packages-btn">
									book now
								</button>
							</div><!--/.about-btn-->
						</div><!--/.single-package-item-txt-->
					</div><!--/.single-package-item-->

				</div><!--/.col-->

				<div class="col-md-3 col-sm-6">
					<div class="single-package-item">
						<img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg" alt="package-place">
						<div class="single-package-item-txt">
							<h3>Nusa Penida<span class="pull-right">$499</span></h3>
							<div class="packages-para">
								<p>
									<span>
										<i class="fa fa-angle-right"></i> 5 daays 6 nights
									</span>
									<i class="fa fa-angle-right"></i>  5 star accomodation
								</p>
								<p>
									<span>
										<i class="fa fa-angle-right"></i>  transportation
									</span>
									<i class="fa fa-angle-right"></i>  food facilities
								</p>
							</div><!--/.packages-para-->
							<div class="packages-review">
								<p>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<span>2544 review</span>
								</p>
							</div><!--/.packages-review-->
							<div class="about-btn">
								<button  class="about-view packages-btn">
									book now
								</button>
							</div><!--/.about-btn-->
						</div><!--/.single-package-item-txt-->
					</div><!--/.single-package-item-->

				</div><!--/.col-->
				<div class="col-md-3 col-sm-6">
					<div class="single-package-item">
						<img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg" alt="package-place">
						<div class="single-package-item-txt">
							<h3>Nusa Penida<span class="pull-right">$499</span></h3>
							<div class="packages-para">
								<p>
									<span>
										<i class="fa fa-angle-right"></i> 5 daays 6 nights
									</span>
									<i class="fa fa-angle-right"></i>  5 star accomodation
								</p>
								<p>
									<span>
										<i class="fa fa-angle-right"></i>  transportation
									</span>
									<i class="fa fa-angle-right"></i>  food facilities
								</p>
							</div><!--/.packages-para-->
							<div class="packages-review">
								<p>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<span>2544 review</span>
								</p>
							</div><!--/.packages-review-->
							<div class="about-btn">
								<button  class="about-view packages-btn">
									book now
								</button>
							</div><!--/.about-btn-->
						</div><!--/.single-package-item-txt-->
					</div><!--/.single-package-item-->

				</div><!--/.col-->
				<div class="col-md-3 col-sm-6">
					<div class="single-package-item">
						<img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg" alt="package-place">
						<div class="single-package-item-txt">
							<h3>Nusa Penida<span class="pull-right">$499</span></h3>
							<div class="packages-para">
								<p>
									<span>
										<i class="fa fa-angle-right"></i> 5 daays 6 nights
									</span>
									<i class="fa fa-angle-right"></i>  5 star accomodation
								</p>
								<p>
									<span>
										<i class="fa fa-angle-right"></i>  transportation
									</span>
									<i class="fa fa-angle-right"></i>  food facilities
								</p>
							</div><!--/.packages-para-->
							<div class="packages-review">
								<p>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<span>2544 review</span>
								</p>
							</div><!--/.packages-review-->
							<div class="about-btn">
								<button  class="about-view packages-btn">
									book now
								</button>
							</div><!--/.about-btn-->
						</div><!--/.single-package-item-txt-->
					</div><!--/.single-package-item-->

				</div><!--/.col-->
				<div class="col-md-3 col-sm-6">
					<div class="single-package-item">
						<img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg" alt="package-place">
						<div class="single-package-item-txt">
							<h3>Nusa Penida<span class="pull-right">$499</span></h3>
							<div class="packages-para">
								<p>
									<span>
										<i class="fa fa-angle-right"></i> 5 daays 6 nights
									</span>
									<i class="fa fa-angle-right"></i>  5 star accomodation
								</p>
								<p>
									<span>
										<i class="fa fa-angle-right"></i>  transportation
									</span>
									<i class="fa fa-angle-right"></i>  food facilities
								</p>
							</div><!--/.packages-para-->
							<div class="packages-review">
								<p>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<span>2544 review</span>
								</p>
							</div><!--/.packages-review-->
							<div class="about-btn">
								<button  class="about-view packages-btn">
									book now
								</button>
							</div><!--/.about-btn-->
						</div><!--/.single-package-item-txt-->
					</div><!--/.single-package-item-->

				</div><!--/.col-->
				<div class="col-md-3 col-sm-6">
					<div class="single-package-item">
						<img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_165210-680x500.jpg" alt="package-place">
						<div class="single-package-item-txt">
							<h3>Nusa Penida<span class="pull-right">$499</span></h3>
							<div class="packages-para">
								<p>
									<span>
										<i class="fa fa-angle-right"></i> 5 daays 6 nights
									</span>
									<i class="fa fa-angle-right"></i>  5 star accomodation
								</p>
								<p>
									<span>
										<i class="fa fa-angle-right"></i>  transportation
									</span>
									<i class="fa fa-angle-right"></i>  food facilities
								</p>
							</div><!--/.packages-para-->
							<div class="packages-review">
								<p>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<span>2544 review</span>
								</p>
							</div><!--/.packages-review-->
							<div class="about-btn">
								<button  class="about-view packages-btn">
									book now
								</button>
							</div><!--/.about-btn-->
						</div><!--/.single-package-item-txt-->
					</div><!--/.single-package-item-->

				</div><!--/.col-->

			</div><!--/.row-->
		</div><!--/.packages-content-->
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-end">
				<li class="page-item disabled">
					<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
				</li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item">
					<a class="page-link" href="#">Next</a>
				</li>
			</ul>
		</nav>
	</div>

</section>
<?php include'footer.php' ?>