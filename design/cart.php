<?php include'header.php' ?>

<nav aria-label="breadcrumb bg-white border">
    <ol class="breadcrumb bg-white container" style="background-color:white;">
        <li class="breadcrumb-item mx-4">
            <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>
<div class="container">
    <div class="row mt-3">
        <div class="col-sm-8">
            <h4>CART ITEM</h4>
            <div class="row mt-3">
                <button type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="col-sm-3">
                    <img src="https://bluepenida.com/wp-content/uploads/2019/05/IMG_20190525_164711-150x150.jpg" alt="">
                </div>
                <div class="col-sm-6">

                    <h4 class="booking-item-title">Fast Boat Transfer Kusamba – Nusa Penida – Kusamba</h4>
                    <p class="booking-item-address">
                        <i class="input-icon field-icon fa">
                            <svg width="15px" height="15px" viewbox="0 0 24 24" version="1.1"
                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->

                                <desc>Created with Sketch.</desc>
                                <defs></defs>
                                <g id="Ico_maps" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                    stroke-linecap="round" stroke-linejoin="round">
                                    <g id="Group" transform="translate(4.000000, 0.000000)" stroke="#666666">
                                        <g id="pin-1" transform="translate(-0.000000, 0.000000)">
                                            <path
                                                d="M15.75,8.25 C15.75,12.471 12.817,14.899 10.619,17.25 C9.303,18.658 8.25,23.25 8.25,23.25 C8.25,23.25 7.2,18.661 5.887,17.257 C3.687,14.907 0.75,12.475 0.75,8.25 C0.75,4.10786438 4.10786438,0.75 8.25,0.75 C12.3921356,0.75 15.75,4.10786438 15.75,8.25 Z"
                                                id="Shape"></path>
                                            <circle id="Oval" cx="8.25" cy="8.25" r="3"></circle>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </i>Kusamba, Klungkung Regency, Bali, Indonesia</p>
                    <p class="booking-item-description">
                        <span>Type tour:
                        </span>
                        Special Date</p>

                    <p class="booking-item-description">
                        <span>Departure date:
                        </span>13/09/2019</p>
                    <p class="booking-item-description">
                        <span>Return date:
                        </span>13/09/2019</p>

                    <div id="st_cart_item1dbed737ea305012221885555e6703d4" class="">
                        <p>
                            <small>Booking Details</small>
                        </p>
                        <div class="cart_border_bottom"></div>
                        <div class="cart_item_group" style="margin-bottom: 10px">
                            <div class="booking-item-description">
                                <p class="booking-item-description">
                                    <span>Adult:
                                    </span>1 x $15,00
                                    <i class="fa fa-long-arrow-right"></i>
                                    $15,00
                                    <br>
                                </p>
                                <p>Total amount: $148,41</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <h4>$15,00</h4>
                </div>
            </div>

        </div>
        <div class="col-sm-4">
            <h4>CART TOTALS</h4>
            <div class="card px-3 mt-3">
                <div class="card-body py-3">
                    <table class="table">
                        <tr>
                            <td>Subtotal</td>
                            <td>$128,04</td>
                        </tr>
                        <tr>
                            <td>Shipping</td>
                            <td width="50%">
                                Free shipping
                                <p>
                                    <button class="btn btn-link" data-toggle="collapse" href="#collapseExample"
                                        role="button" aria-expanded="false" aria-controls="collapseExample">
                                        Calculate shipping
                                    </button>
                                </p>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body">
                                        <div class="form-group">
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="exampleFormControlInput1"
                                                placeholder="name@example.com">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="exampleFormControlInput1"
                                                placeholder="name@example.com">
                                        </div>
                                        <button type="submit" class="btn btn-primary mb-2 mt-2">Confirm identity</button>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Subtotal</td>
                            <td>$128,04</td>
                        </tr>
                    </table>
                    <a href="checkout.php">
                        <button type="submit" class="btn btn-primary mb-2 mt-2">Checkout</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-sm-12">
            <form class="form-inline">
                <div class="form-group mx-sm-3 mb-2">
                    <input type="text" class="form-control" placeholder="PasswCoupon Code">
                </div>
                <button type="submit" class="btn btn-primary mb-2 mt-2">Apply Coupon</button>
                <button type="submit" class="btn btn-primary mb-2 mt-2">Update Cart</button>
            </form>
        </div>
    </div>
</div>

<?php include'footer.php' ?>