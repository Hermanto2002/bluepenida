<?php include'header.php' ?>


<section id="home" class="about-us-blog">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 style="color:white; font-weight: bold;">BLOG</h1>
			</div>	
		</div><!--/.row-->
	</div><!--/.container-->
</section><!--/.about-us-->
<nav aria-label="breadcrumb bg-white border">
    <ol class="breadcrumb bg-white container" style="background-color:white;">
        <li class="breadcrumb-item mx-4">
            <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>
<section>
	<div class="container">
		<div class="row px-3">
			<div class="col-md-9">
				<div class="row ">
					<div class="col-md-12">
						<div class="single-package-item">
							<img src="https://bluepenida.com/wp-content/uploads/2019/07/nusa-penida-3631287_1920-870x500.jpg" alt="package-place">
							<div class="single-package-item-txt">
								<h3>The beauty of Mount Agung is seen from Nusa Penida</h3>
								<div class="packages-para">
									<p>
										<span>
											<img style="width: 30%;" src="https://secure.gravatar.com/avatar/fc91e3ae38d33a4c2cb1e88da7002c04?s=30&d=mm&r=g"> by Nyoman
										</span>
										<span>JUL 6, 2019</span>
										<span>NO COMMENT</span>
									</p>
									<p>
										If you visit Nusa Penida there are many beauties that you can meet.  From beautiful beaches, clear springs and graceful, charming hills. Apart from that beauty, from the island of Nusa Penida you can see the beauty of Mount Agung which is beautifully towering. There you will see a class of how the Mount Agung […]
									</p>
								</div><!--/.packages-para-->
								<div class="about-btn">
									<button  class="about-view packages-btn">
										READ MORE
									</button>
								</div><!--/.about-btn-->
							</div><!--/.single-package-item-txt-->
						</div><!--/.single-package-item-->

					</div><!--/.col-->
					<div class="col-md-12">
						<div class="single-package-item">
							<img src="https://bluepenida.com/wp-content/uploads/2019/07/nusa-penida-3631287_1920-870x500.jpg" alt="package-place">
							<div class="single-package-item-txt">
								<h3>The beauty of Mount Agung is seen from Nusa Penida</h3>
								<div class="packages-para">
									<p>
										<span>
											<img style="width: 30%;" src="https://secure.gravatar.com/avatar/fc91e3ae38d33a4c2cb1e88da7002c04?s=30&d=mm&r=g"> by Nyoman
										</span>
										<span>JUL 6, 2019</span>
										<span>NO COMMENT</span>
									</p>
									<p>
										If you visit Nusa Penida there are many beauties that you can meet.  From beautiful beaches, clear springs and graceful, charming hills. Apart from that beauty, from the island of Nusa Penida you can see the beauty of Mount Agung which is beautifully towering. There you will see a class of how the Mount Agung […]
									</p>
								</div><!--/.packages-para-->
								<div class="about-btn">
									<button  class="about-view packages-btn">
										READ MORE
									</button>
								</div><!--/.about-btn-->
							</div><!--/.single-package-item-txt-->
						</div><!--/.single-package-item-->

					</div><!--/.col-->
					<div class="col-md-12">
						<div class="single-package-item">
							<img src="https://bluepenida.com/wp-content/uploads/2019/07/nusa-penida-3631287_1920-870x500.jpg" alt="package-place">
							<div class="single-package-item-txt">
								<h3>The beauty of Mount Agung is seen from Nusa Penida</h3>
								<div class="packages-para">
									<p>
										<span>
											<img style="width: 30%;" src="https://secure.gravatar.com/avatar/fc91e3ae38d33a4c2cb1e88da7002c04?s=30&d=mm&r=g"> by Nyoman
										</span>
										<span>JUL 6, 2019</span>
										<span>NO COMMENT</span>
									</p>
									<p>
										If you visit Nusa Penida there are many beauties that you can meet.  From beautiful beaches, clear springs and graceful, charming hills. Apart from that beauty, from the island of Nusa Penida you can see the beauty of Mount Agung which is beautifully towering. There you will see a class of how the Mount Agung […]
									</p>
								</div><!--/.packages-para-->
								<div class="about-btn">
									<button  class="about-view packages-btn">
										READ MORE
									</button>
								</div><!--/.about-btn-->
							</div><!--/.single-package-item-txt-->
						</div><!--/.single-package-item-->

					</div><!--/.col-->
					<div class="col-md-12">
						<div class="single-package-item">
							<img src="https://bluepenida.com/wp-content/uploads/2019/07/nusa-penida-3631287_1920-870x500.jpg" alt="package-place">
							<div class="single-package-item-txt">
								<h3>The beauty of Mount Agung is seen from Nusa Penida</h3>
								<div class="packages-para">
									<p>
										<span>
											<img style="width: 30%;" src="https://secure.gravatar.com/avatar/fc91e3ae38d33a4c2cb1e88da7002c04?s=30&d=mm&r=g"> by Nyoman
										</span>
										<span>JUL 6, 2019</span>
										<span>NO COMMENT</span>
									</p>
									<p>
										If you visit Nusa Penida there are many beauties that you can meet.  From beautiful beaches, clear springs and graceful, charming hills. Apart from that beauty, from the island of Nusa Penida you can see the beauty of Mount Agung which is beautifully towering. There you will see a class of how the Mount Agung […]
									</p>
								</div><!--/.packages-para-->
								<div class="about-btn">
									<button  class="about-view packages-btn">
										READ MORE
									</button>
								</div><!--/.about-btn-->
							</div><!--/.single-package-item-txt-->
						</div><!--/.single-package-item-->

					</div><!--/.col-->
				</div>
				<nav aria-label="Page navigation example">
					<ul class="pagination justify-content-end">
						<li class="page-item disabled">
							<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
						</li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item">
							<a class="page-link" href="#">Next</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="col-md-3">
				<div class="single-tab-select-box">
					<div class="travel-check-icon travel-search-icon">
						<form action="#">
							<input type="search" name="check_out" class="form-control" placeholder="Search">
						</form>
					</div><!-- /.travel-check-icon -->
				</div><!--/.single-tab-select-box-->
				<h4>ABOUT BLUE PENIDA</h4> <br>
				<p>Bluepenida.com is a website that provides services to tourists who want to go to Nusa Penida. In its development Bluepenida will also open its services more broadly. Bluepenida.com works with speedboat operators from and to Nusa Penida, drivers, tour guides, activities and lodging.

					Our motto Bluepenida.com is easy and fun. Because by using the services of Bluepenida.com, your tour will go well as planned, because our service features are easily understood with complete information.

				Besides being easy, bluepenida.com makes it happy or fun, because the bluepenida.com also provides comfort, so that tourists who use bluepenida.com are happy. Because the purpose of the vacationer is to have fun, eliminate excitement, saturate and return to enthusiasm.</p>
				<br>
				<h4>Recent Posts</h4> <br>
				<div class="card mb-4">
					<div class="row no-gutters">
						<div class="col-md-5">
							<img src="https://bluepenida.com/wp-content/uploads/2019/06/DJI_0026-100x100.jpg" class="card-img" alt="...">
						</div>
						<div class="col-md-7">
							<div class="card-body">
								<h5 class="card-title">Card title</h5>
								<p class="card-text">Bluepenida.com is a website that provides services to tourists...</p>
								<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-4">
					<div class="row no-gutters">
						<div class="col-md-5">
							<img src="https://bluepenida.com/wp-content/uploads/2019/06/DJI_0026-100x100.jpg" class="card-img" alt="...">
						</div>
						<div class="col-md-7">
							<div class="card-body">
								<h5 class="card-title">Card title</h5>
								<p class="card-text">Bluepenida.com is a website that provides services to tourists...</p>
								<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-4">
					<div class="row no-gutters">
						<div class="col-md-5">
							<img src="https://bluepenida.com/wp-content/uploads/2019/06/DJI_0026-100x100.jpg" class="card-img" alt="...">
						</div>
						<div class="col-md-7">
							<div class="card-body">
								<h5 class="card-title">Card title</h5>
								<p class="card-text">Bluepenida.com is a website that provides services to tourists...</p>
								<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
							</div>
						</div>
					</div>
				</div>
				<div class="card mb-4">
					<div class="row no-gutters">
						<div class="col-md-5">
							<img src="https://bluepenida.com/wp-content/uploads/2019/06/DJI_0026-100x100.jpg" class="card-img" alt="...">
						</div>
						<div class="col-md-7">
							<div class="card-body">
								<h5 class="card-title">Card title</h5>
								<p class="card-text">Bluepenida.com is a website that provides services to tourists...</p>
								<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include'footer.php' ?>