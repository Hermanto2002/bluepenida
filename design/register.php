<?php include'head.php' ?>

<section>
    <div class="container">
        <div class="offset-md-3 col-md-6" style="margin-top:70px;">
            <div class="single-package-item">
                <div class="single-package-item-txt">
                    <h3>REGISTER</h3>
                    <!--/.packages-para-->
                    <form class="mx-5">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Full Name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1"
                                placeholder="Password">
                        </div>
                        <label for="exampleInputPassword1">Select User Type</label>

                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1"
                                value="option1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                                Normal User
                            </label>
                        </div>
                        <div class="form-check mb-3">

                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2"
                                value="option2">
                            <label class="form-check-label" for="exampleRadios2">
                                Partner User
                            </label>
                        </div>
                        <label for="term">
                            <input id="term" type="checkbox" name="term" class="mr5"> I have read and accept the <a
                                class="st-link" href="https://bluepenida.com/?page_id=3">Terms and Privacy Policy</a>
                            <span class="checkmark fcheckbox"></span>
                        </label>
                        <button type="submit" class="btn btn-primary btn-block">Signup</button>
                    </form>
                </div>
                <div class="single-package-item-txt">
                    <p class="text-center mb-1">or continue with</p>
                    <p class="text-center">
                        <button type="button" class="btn btn-secondary">Facebook</button>
                        <button type="button" class="btn btn-secondary">Google</button>
                        <button type="button" class="btn btn-secondary">Twitter</button>
                    </p>
                    <p class="text-center mt-3">Do you have an account? <a href="">Login</a></p>
                </div>
                <!--/.single-package-item-txt-->
            </div>
            <!--/.single-package-item-->

        </div>
    </div>
    <!--/.container-->

</section>
<!--/.gallery-->

<?php include'foot.php' ?>