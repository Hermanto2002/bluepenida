<!--subscribe start-->
<section id="subs" class="subscribe">
	<div class="container">
		<div class="subscribe-title text-center">
			<h2>
				Join our Subscribers List to Get Regular Update
			</h2>
			<p>
				Subscribe Now. We will send you Best offer for your Trip 
			</p>
		</div>
		<form>
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="custom-input-group">
						<input type="email" class="form-control" placeholder="Enter your Email Here">
						<button class="appsLand-btn subscribe-btn">Subscribe</button>
						<div class="clearfix"></div>
						<i class="fa fa-envelope"></i>
					</div>

				</div>
			</div>
		</form>
	</div>

</section>
<!--subscribe end-->

<!-- footer-copyright start -->
<footer  class="footer-copyright">
	<div class="container">
		<div class="footer-content">
			<div class="row">

				<div class="col-6 col-md-3">
					<div class="single-footer-item">
						<div class="footer-logo">
							<img style="width: 80%" src="assets/images/bluepenida-logo.png">
						</div>
					</div><!--/.single-footer-item-->
				</div><!--/.col-->

				<div class="col-6 col-md-3">
					<div class="single-footer-item">
						<h2>NEED HELP ?</h2>
						<ul class="list-group list-group-flush bg-transparent">
							<li style="background-color: transparent;" class="list-group-item">Call Us
								<p><a href="#">+62 8123 8462 468</a></p>
								<p><a href="#">+62 8786 6891 288</a></p>
							</li>
							<li style="background-color: transparent;" class="list-group-item">Email For Us
								<p><a href="#">contact@bluepenida.com</a></p>
							</li>
						</ul>
					</div><!--/.single-footer-item-->

				</div><!--/.col-->

				<div class="col-6 col-md-3">
					<div class="single-footer-item">
						<h2>COMPANY</h2>
						<div class="single-footer-txt">
							<p><a href="#">About Us</a></p>
							<p><a href="#">Term & Condition</a></p>
							<p><a href="#">Tourist Information</a></p>
						</div><!--/.single-footer-txt-->
					</div><!--/.single-footer-item-->
				</div><!--/.col-->

				<div class="col-6 col-md-3">
					<div class="single-footer-item text-center ">
						<h2 class="text-left">SETTING</h2>
						<div class="single-footer-txt text-left travel-select-icon">
							<select class="form-control ">
								<option value="default">USD</option><!-- /.option-->
								<option value="2">EUR</option><!-- /.option-->
								<option value="4">AUD</option><!-- /.option-->
								<option value="8">IDR</option><!-- /.option-->
							</select><!-- /.select-->
						</div><!--/.single-footer-txt-->
					</div><!--/.single-footer-item-->
				</div><!--/.col-->

			</div><!--/.row-->

		</div><!--/.footer-content-->
		<hr>
		<div class="foot-icons ">
			<ul class="footer-social-links list-inline list-unstyled">
				<li><a href="#" target="_blank" class="foot-icon-bg-1"><i class="fab fa-facebook-f"></i></a></li>
				<li><a href="#" target="_blank" class="foot-icon-bg-2"><i class="fab fa-twitter"></i></a></li>
				<li><a href="#" target="_blank" class="foot-icon-bg-3"><i class="fab fa-instagram"></i></i></a></li>
				<li><a href="#" target="_blank" class="foot-icon-bg-4"><i class="fab fa-whatsapp"></i></i></i></a></li>
				<li><a href="#" target="_blank" class="foot-icon-bg-4"><i class="fab fa-whatsapp"></i></i></i></a></li>
			</ul>
			<p>&copy; 2019 <a href="https://www.bluepenida.com">Bluepenida</a>. All Right Reserved</p>

		</div><!--/.foot-icons-->
		<div id="scroll-Top">
			<i class="fa fa-angle-double-up return-to-top" id="scroll-top" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back to Top" aria-hidden="true"></i>
		</div><!--/.scroll-Top-->
	</div><!-- /.container-->

</footer><!-- /.footer-copyright-->
<!-- footer-copyright end -->
<?php include'foot.php' ?>
