<?php include'head.php' ?>


<body>
	<section style="background-color:#1A2B48;">
		<div class="container">
			<div class="row mx-2">
				<div class="col-xs-12">
					<p class="text-right">
						<a type="button" href="login.php" class="btn btn-link" style="color:white;">
							Login
						</a>|
						<a type="button" href="register.php" class="btn btn-link" style="color:white;">
							Signup
						</a>
					</p>
				</div>
			</div>
		</div>
	</section>
	<nav class="navbar navbar-default navbar-sticky bootsnav">
		<div class="container">

			<!-- Start Atribute Navigation -->
			<div class="attr-nav">
				<ul>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa-2x fa fa-shopping-bag"></i>
							<span class="badge">3</span>
						</a>
						<ul class="dropdown-menu cart-list">
							<li>
								<a href="#" class="photo"><img src="IMAGE_ADDRESS" class="cart-thumb" alt="" /></a>
								<h6><a href="#">Delica omtantur </a></h6>
								<p>2x - <span class="price">$99.99</span></p>
							</li>
							<li>
								<a href="#" class="photo"><img src="IMAGE_ADDRESS" class="cart-thumb" alt="" /></a>
								<h6><a href="#">Delica omtantur </a></h6>
								<p>2x - <span class="price">$99.99</span></p>
							</li>
							-- More List --
							<li class="total">
								<span class="pull-right"><strong>Total</strong>: $0.00</span>
								<a href="#" class="btn btn-default btn-cart">Cart</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			<!-- End Atribute Navigation -->


			<!-- Start Header Navigation -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
					<i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand" href="index.php"><img style="width: 100%;" src="assets/images/logo.png"
						class="logo" alt=""></a>
			</div>
			<!-- End Header Navigation -->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbar-menu">
				<ul class="nav navbar-nav navbar-center">
					<li><a href="#">HOME</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">TOUR PACKAGES</a>
						<ul class="dropdown-menu">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tour Packages Nusa Penida</a>
								<ul class="dropdown-menu">
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tour Packages Nusa Penida</a>
								<ul class="dropdown-menu">
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tour Packages Nusa Penida</a>
								<ul class="dropdown-menu">
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
									<li><a href="detailtour.php">Half Day Tour West Nusa Penida</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">WATERSPORT</a>
						<ul class="dropdown-menu">
							<li>
								<a href="detailtour.php" class="dropdown-toggle" data-toggle="dropdown">Tour Packages
									Nusa Penida</a>
							</li>
							<li>
								<a href="detailtour.php" class="dropdown-toggle" data-toggle="dropdown">Tour Packages
									Nusa Penida</a>
							</li>
							<li>
								<a href="detailtour.php" class="dropdown-toggle" data-toggle="dropdown">Tour Packages
									Nusa Penida</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">FAST BOAT TRANSFER</a>
						<ul class="dropdown-menu">
							<li>
								<a href="detailtour.php" class="dropdown-toggle" data-toggle="dropdown">Tour Packages
									Nusa Penida</a>
							</li>
							<li>
								<a href="detailtour.php" class="dropdown-toggle" data-toggle="dropdown">Tour Packages
									Nusa Penida</a>
							</li>
							<li>
								<a href="detailtour.php" class="dropdown-toggle" data-toggle="dropdown">Tour Packages
									Nusa Penida</a>
							</li>
						</ul>
					</li>
					<li><a href="blog.php">TOURIST INFORMATION</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>