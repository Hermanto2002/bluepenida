<section  class="travel-box">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-travel-boxes">
					<div id="desc-tabs" class="desc-tabs">

						<ul class="nav nav-tabs" role="tablist" id="myDIV">

							<li role="presentation" class="tombol active">
								<a href="#tours" aria-controls="tours" role="tab" data-toggle="tab">
									<i class="fa fa-tree"></i>
									Tours
								</a>
							</li>

							<li role="presentation" class="tombol">
								<a href="#hotels" aria-controls="hotels" role="tab" data-toggle="tab">
									<i class="fa fa-building"></i>
									Room
								</a>
							</li>

							<li role="presentation" class="tombol">
								<a href="#flights" aria-controls="flights" role="tab" data-toggle="tab">
									<i class="fas fa-ship"></i>
									Boat
								</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">

							<div role="tabpanel" class="tab-pane active fade in" id="tours">
								<div class="tab-para">

									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">

												<h2>destination</h2>

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your destination country</option><!-- /.option-->

														<option value="turkey">turkey</option><!-- /.option-->

														<option value="russia">russia</option><!-- /.option-->
														<option value="egept">egypt</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->

											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>From</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_in" class="form-control" data-toggle="datepicker" placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>To</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_out" class="form-control"  data-toggle="datepicker" placeholder="22 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->



										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>Person</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="4">4</option><!-- /.option-->
														<option value="8">8</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

									</div><!--/.row-->

									<div class="row">

										<div class="clo-sm-7">
											<div class="about-btn travel-mrt-0 pull-right">
												<button  class="about-view travel-btn">
													search	
												</button><!--/.travel-btn-->
											</div><!--/.about-btn-->
										</div><!--/.col-->

									</div><!--/.row-->

								</div><!--/.tab-para-->

							</div><!--/.tabpannel-->

							<div role="tabpanel" class="tab-pane fade in" id="hotels">
								<div class="tab-para">

									<div class="row">
										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>check in</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_in" class="form-control" data-toggle="datepicker" placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-3 col-sm-4">
											<div class="single-tab-select-box">
												<h2>check out</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="check_out" class="form-control"  data-toggle="datepicker" placeholder="22 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>Adult</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="3">3</option><!-- /.option-->
														<option value="4">4</option><!-- /.option-->
														<option value="5">5</option><!-- /.option-->
													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-2 col-md-1 col-sm-4">
											<div class="single-tab-select-box">
												<h2>Child</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">1</option><!-- /.option-->

														<option value="2">2</option><!-- /.option-->

														<option value="3">3</option><!-- /.option-->
														<option value="4">4</option><!-- /.option-->
														<option value="5">5</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

									</div><!--/.row-->

									<div class="row">
										<div class="col-sm-5"></div><!--/.col-->
										<div class="clo-sm-7">
											<div class="about-btn travel-mrt-0 pull-right">
												<button  class="about-view travel-btn">
													search	
												</button><!--/.travel-btn-->
											</div><!--/.about-btn-->
										</div><!--/.col-->

									</div><!--/.row-->

								</div><!--/.tab-para-->

							</div><!--/.tabpannel-->

							<div role="tabpanel" class="tab-pane fade in" id="flights">
								<div class="tab-para">
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">

												<h2>from</h2>

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your location</option><!-- /.option-->

														<option value="turkey">turkey</option><!-- /.option-->

														<option value="russia">russia</option><!-- /.option-->
														<option value="egept">egypt</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">

												<h2>to</h2>

												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">enter your destination location</option><!-- /.option-->

														<option value="istambul">istambul</option><!-- /.option-->

														<option value="mosko">mosko</option><!-- /.option-->
														<option value="cairo">cairo</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->

											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">
												<h2>Passanger</h2>
												<div class="travel-select-icon">
													<select class="form-control ">

														<option value="default">5</option><!-- /.option-->

														<option value="10">10</option><!-- /.option-->

														<option value="15">15</option><!-- /.option-->
														<option value="20">20</option><!-- /.option-->

													</select><!-- /.select-->
												</div><!-- /.travel-select-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->

										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">
												<h2>departure</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="departure" class="form-control" data-toggle="datepicker"
														placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->
										<div class="col-lg-4 col-md-4 col-sm-12" id="LicenseCustomer" style="display:none">
											<div class="single-tab-select-box">
												<h2>return</h2>
												<div class="travel-check-icon">
													<form action="#">
														<input type="text" name="departure" class="form-control" data-toggle="datepicker"
														placeholder="12 -01 - 2017 ">
													</form>
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="single-tab-select-box">
												<h2></h2>
												<div class="travel">
													<div class="single-trip-circle">
														<input type="checkbox" id="_My.notFinal"  name="radio" />
														<label for="radio01" style="margin-left: 10px: ">
															<span class="round-boarder">
																<span class="round-boarder1"></span>
															</span>&nbsp;&nbsp;Return
														</label>
													</div><!--/.single-trip-circle-->
												</div><!-- /.travel-check-icon -->
											</div><!--/.single-tab-select-box-->
										</div><!--/.col-->




									</div><!--/.row-->

									<div class="row">

										<div class="clo-sm-5">
											<div class="about-btn pull-right">
												<button  class="about-view travel-btn">
													search	
												</button><!--/.travel-btn-->
											</div><!--/.about-btn-->
										</div><!--/.col-->

									</div><!--/.row-->

								</div>

							</div><!--/.tabpannel-->

						</div><!--/.tab content-->
					</div><!--/.desc-tabs-->
				</div><!--/.single-travel-box-->
			</div><!--/.col-->
		</div><!--/.row-->
	</div><!--/.container-->

		</section><!--/.travel-box-->