<script src="assets/js/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script  src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script  src="assets/js/bootstrap.min.js"></script>

<!-- bootsnav js -->
<script src="assets/js/bootsnav.js"></script>

<!-- jquery.filterizr.min.js -->
<script src="assets/js/jquery.filterizr.min.js"></script>

<script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--jquery-ui.min.js-->
<script src="assets/js/jquery-ui.min.js"></script>

<!-- counter js -->
<script src="assets/js/jquery.counterup.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>

<!--owl.carousel.js-->
<script  src="assets/js/owl.carousel.min.js"></script>

<!-- jquery.sticky.js -->
<script src="assets/js/jquery.sticky.js"></script>

<!--datepicker.js-->
<script  src="assets/js/datepicker.js"></script>

<!--Custom JS-->
<script src="assets/js/custom.js"></script>
<script src="assets/js/utils.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<script>
	$(function () {
		
		$("#rateYo").rateYo({
			rating: 3.2,
			spacing: "1px",
			starWidth: "20px"
		});
	});
</script>
<script type="text/javascript">
	function toggleCheckbox() {
		var lfckv = document.getElementById("_My.notFinal").checked;
		if (lfckv) {
			document.getElementById("LicenseCustomer").style.display = "block";
		} else {
			document.getElementById("LicenseCustomer").style.display = "none";
		}
	}

	document.getElementById("_My.notFinal").onclick = toggleCheckbox;
</script>
</body>

</html>